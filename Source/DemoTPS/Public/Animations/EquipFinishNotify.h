// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "BaseAnimNotify.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "EquipFinishNotify.generated.h"

UCLASS()
class DEMOTPS_API UEquipFinishNotify : public UBaseAnimNotify
{
	GENERATED_BODY()
};
