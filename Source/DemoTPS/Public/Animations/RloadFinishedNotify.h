// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "Animations/BaseAnimNotify.h"
#include "RloadFinishedNotify.generated.h"

UCLASS()
class DEMOTPS_API URloadFinishedNotify : public UBaseAnimNotify
{
	GENERATED_BODY()
};
