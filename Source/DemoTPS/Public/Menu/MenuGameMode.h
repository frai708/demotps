// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MenuGameMode.generated.h"

/**
 * 
 */
UCLASS()
class DEMOTPS_API AMenuGameMode : public AGameModeBase
{
	GENERATED_BODY()
public:
	AMenuGameMode();
};
