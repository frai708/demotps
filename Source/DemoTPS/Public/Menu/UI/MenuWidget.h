// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "BaseWidget.h"
#include "DemoTPSCoreTypes.h"
#include "MenuWidget.generated.h"

class UButton;
class UHorizontalBox;
class UTPSGameInstance;
class AMainGameMode;
class ULevelItemWidget;
class ULevelSettingsWidget;
class USoundCue;
class UVerticalBox;
UCLASS()
class DEMOTPS_API UMenuWidget : public UBaseWidget
{
	GENERATED_BODY()
protected:
	UPROPERTY(meta = (BindWidget))
	UButton* StartGameButton;

	UPROPERTY(meta = (BindWidget))
	UButton* ShowLevelButton;

	UPROPERTY(meta = (BindWidget))
	UButton* QuitGameButton;

	UPROPERTY(meta = (BindWidget))
	UVerticalBox* LevelShowBox;

	UPROPERTY(meta = (BindWidget))
	UVerticalBox* LevelItemBox;

	UPROPERTY(meta = (BindWidget))
	UVerticalBox* LevelSettingsBox;

	UPROPERTY(meta = (BindWidget))
	ULevelSettingsWidget* PlayerCount;

	UPROPERTY(meta = (BindWidget))
	ULevelSettingsWidget* RoundCount;

	UPROPERTY(meta = (BindWidget))
	ULevelSettingsWidget* TimeCount;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="UI")
	TSubclassOf<UUserWidget> LevelItemWidgetClass;

	UPROPERTY(Transient, meta = (BindWidgetAnim))
	UWidgetAnimation* HideAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Sound")
	USoundCue* StartGameSound;

	virtual void NativeOnInitialized() override;
	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;

private:

	UPROPERTY()
	TArray<ULevelItemWidget*> LevelItemWidgets; 
	
	UFUNCTION()
	void OnStartGame();

	UFUNCTION()
	void OnShowItem();

	UFUNCTION()
	void OnQuitGame();

	void InitLevelItems();
	void OnLevelSelected(const FLevelData& Data);
	UTPSGameInstance* GetTPSGameInstance() const;
};
