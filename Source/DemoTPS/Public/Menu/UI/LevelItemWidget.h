// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DemoTPSCoreTypes.h"
#include "LevelItemWidget.generated.h"


class UButton;
class UTextBlock;
class UImage;
UCLASS()
class DEMOTPS_API ULevelItemWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	FOnLevelSelectedSignature OnLevelSelected;

	void SetLevelData(const FLevelData& Data);
	FLevelData GetLevelData() const {return LevelData;}

	void SetSelected(bool IsSelected);
	
protected:
	UPROPERTY(meta = (BindWidget))
	UButton* LevelSelectButton;

	UPROPERTY(meta = (BindWidget))
	UTextBlock*  LevelNameTextBlock;

	UPROPERTY(meta = (BindWidget))
	UImage* FrameImage;
	
	UPROPERTY(meta = (BindWidget))
	UImage* LevelImage;

	virtual void NativeOnInitialized() override;

private:
	FLevelData LevelData;

	UFUNCTION()
	void OnLevelItemClicked();

	UFUNCTION()
	void OnLevelItemHovered();

	UFUNCTION()
	void OnLevelItemUnHovered();
};
