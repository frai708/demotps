// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "UI/BaseWidget.h"
#include "LevelSettingsWidget.generated.h"

class USlider;
class UTextBlock;
UCLASS()
class DEMOTPS_API ULevelSettingsWidget : public UBaseWidget
{
	GENERATED_BODY()

public:
	
	int GetCount() const;

protected:
	UPROPERTY(meta = (BindWidget))
	USlider* S_Amount;
		
	UPROPERTY(meta = (BindWidget))
	UTextBlock* TB_Count;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* TB_SettingInformation;
		
	UPROPERTY(EditInstanceOnly,category = "Text", meta = (AllowPrivateAccess = "true"))
	FText PlayerAmountText;

	UPROPERTY(EditInstanceOnly, category = "Slider", meta = (AllowPrivateAccess = "true"))
	int MinValue;

	UPROPERTY(EditInstanceOnly, category = "Slider", meta = (AllowPrivateAccess = "true"))
	int MaxValue;

	UPROPERTY(EditInstanceOnly, category = "Slider", meta = (AllowPrivateAccess = "true"))
	int SliderValue;


	virtual void NativeOnInitialized();

	UFUNCTION()
	void OnChanged(float Value);
};
