// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "DemoTPSCoreTypes.h"
#include "TPSGameInstance.generated.h"

class USoundClass;
UCLASS()
class DEMOTPS_API UTPSGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	FString TestString = "Hello Game!";

	FLevelData GetStartupLevel() const {return StartupLevel;}
	void SetStartupLevel(const FLevelData& Data) {StartupLevel = Data;}

	TArray<FLevelData> GetLevelsData() const{return LevelsData;}
	
	FName GetMenuLevelName() const {return MenuLevelName;}

	void ToggleVolume();
	
protected:

	UPROPERTY(EditDefaultsOnly, Category = "Game", meta = (ToolTip = "Level name must be unique!"))
	TArray<FLevelData> LevelsData;
		
	UPROPERTY(EditDefaultsOnly, Category = "Game")
	FName MenuLevelName = NAME_None;

	UPROPERTY(EditDefaultsOnly, Category = "Game", meta = (ToolTip = "Level name must be unique!"))
	USoundClass* MasterSoundClass;

private:
	UPROPERTY(EditDefaultsOnly, Category = "Game")
	FLevelData StartupLevel;
};
