// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "Weapon/BaseWeapon.h"
#include "LauncherWeapon.generated.h"

class AProjectile;
class USoundCue;
UCLASS()
class DEMOTPS_API ALauncherWeapon : public ABaseWeapon
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	TSubclassOf<AProjectile> ProjectileClass;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Sound")
	USoundCue* NoAmmoSound;

	virtual void StartFire() override;
	virtual void StopFire() override;
	virtual void MakeShot() override;

};
