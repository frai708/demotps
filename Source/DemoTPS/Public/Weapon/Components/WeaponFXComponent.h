// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DemoTPSCoreTypes.h"
#include "WeaponFXComponent.generated.h"

class UNiagaraSystem;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DEMOTPS_API UWeaponFXComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UWeaponFXComponent();

	void PlayImpactFX(const FHitResult& hit);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category ="VFX")
	TMap<UPhysicalMaterial*, FImpactData> ImpactDataMap;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category ="VFX")
	FImpactData DefaultImpactData;

};
