// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "Weapon/BaseWeapon.h"
#include "RifleWeapon.generated.h"

/**
 * 
 */
class UWeaponFXComponent;
class UNiagaraSystem;
class UNiagaraComponent;
class UNiagaraSystem;
class UAudioComponent;

UCLASS()
class DEMOTPS_API ARifleWeapon : public ABaseWeapon
{
	GENERATED_BODY()

public:
	ARifleWeapon();
	virtual void StartFire() override;
	virtual void StopFire() override;
	virtual void Zoom(bool Enabled) override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float TimeBetweenShots = 0.1f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite,  meta = (AllowPrivateAccess = "true"))
	float BulletSpreed = 1.5f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite,  meta = (AllowPrivateAccess = "true"))
	float DamageAmount = 10.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite,  meta = (AllowPrivateAccess = "true"))
	float FOVZoomAngle = 50.f;

	/*UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	bool bIsShootGun = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	bool bIsAuto = false;*/

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), meta = (EditCondition = "bIsShootGun"))
	float ShotgunSpreed = 100.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), meta = (EditCondition = "bIsShootGun"))
	int BulletNumber = 5.f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite,  Category = "VFX")
	UNiagaraSystem* TraceFX;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite,  Category = "VFX")
	FString TraceTargetName = "TraceTarget";
	
	UPROPERTY(VisibleAnywhere, Category="VFX")
	UWeaponFXComponent* WeaponFXComponent;

	virtual void BeginPlay() override;
	virtual void MakeShot() override;
	virtual bool GetTraceData(FVector& TraceStart, FVector& TraceEnd) const override;
	
private:
	FTimerHandle ShotTimerHandle;
	UPROPERTY()
	UNiagaraComponent* MuzzleFXComponent;
	UPROPERTY()
	UAudioComponent* FireAudioComponent;
	float DefaultCameraFOV = 90.f;
	
	void SingleShot(FHitResult& Hit,  FVector& End);
	void MakeDamage(const FHitResult& HitResult);
	void InitFX();
	void SetFXActive(bool IsActive);
	void SpawnTraceFx(const FVector& TraceStart, const FVector& TraceEnd);
	void SpawnSingleShotEffects();
	AController* GetController() const;
};
