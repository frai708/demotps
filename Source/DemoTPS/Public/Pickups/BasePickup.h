// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BasePickup.generated.h"

class USphereComponent;
class USoundCue;
UCLASS()
class DEMOTPS_API ABasePickup : public AActor
{
	GENERATED_BODY()
	
public:	
	ABasePickup();
protected:

	UPROPERTY(VisibleAnywhere, Category="Pickup")
	USphereComponent* SphereComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
	float RespawnTime = 5.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundCue* PickupTakingSound;

	virtual void BeginPlay() override;
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

public:	

	virtual void Tick(float DeltaTime) override;


	bool CouldBeTaken() const;

private:

	FTimerHandle RespawnTimerHandle;
	float RotationYaw = 0.f;

	virtual bool GivePickupTo(APawn* PlayerPawn);

	void PickupWasTaken();
	void Respawn();
	void GenerateRotationYaw();
};
