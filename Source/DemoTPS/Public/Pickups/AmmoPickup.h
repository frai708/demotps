// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "Pickups/BasePickup.h"
#include "AmmoPickup.generated.h"

class ABaseWeapon;
UCLASS()
class DEMOTPS_API AAmmoPickup : public ABasePickup
{
	GENERATED_BODY()
protected:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Pickup", meta = (ClampMin = "1.0", ClampMax = "10.0"))
	int32 ClipAmount = 10;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Pickup")
	//TSubclassOf<ABaseWeapon> WeaponType;

private:
	
	virtual bool GivePickupTo(APawn* PlayerPawn) override;

};
