// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "BasePickup.h"
#include "HealthPickup.generated.h"

UCLASS()
class DEMOTPS_API AHealthPickup : public ABasePickup
{
	GENERATED_BODY()
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Pickup", meta = (ClampMin = "1.0", ClampMax="100.0"))
	float HealthAmount = 100.f;

private:
	virtual bool GivePickupTo(APawn* PlayerPawn) override;
};
