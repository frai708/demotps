// Demo project for portfolio. Lev Lobachev 

#pragma once

//#include "CoreMinimal.h"
#include "Pickups/BasePickup.h"
#include "WeaponPickup.generated.h"

class USkeletalMeshComponent;
class ABaseWeapon;
UCLASS()
class DEMOTPS_API AWeaponPickup : public ABasePickup
{
	GENERATED_BODY()

public:
	AWeaponPickup();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
	TSubclassOf<ABaseWeapon> WeaponType;



	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Mesh", meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* WeaponMesh;

	virtual void BeginPlay() override;

private:
	virtual bool GivePickupTo(APawn* PlayerPawn) override;
	
};
