// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "NeedAmmoDecorator.generated.h"

class ABaseWeapon;
UCLASS()
class DEMOTPS_API UNeedAmmoDecorator : public UBTDecorator
{
	GENERATED_BODY()
public:
		UNeedAmmoDecorator();

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "AI")
	TSubclassOf<ABaseWeapon> WeaponType;
	
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
