// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TPSAIController.generated.h"

class UNPCAIPerceptionComponent;
class URespawnComponent;
UCLASS()
class DEMOTPS_API ATPSAIController : public AAIController
{
	GENERATED_BODY()
public:
	ATPSAIController();
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Components")
	UNPCAIPerceptionComponent* NPCAIPerceptionComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Components")
	URespawnComponent* RespawnComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Components")
	FName FocusOnKeyName = "EnemyActor";
	
	virtual void OnPossess(APawn* InPawn) override;
	virtual void Tick(float DeltaSeconds) override;

private:
	AActor* GetFocusOnActor();
};
