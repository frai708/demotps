// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "Player/BaseCharacter.h"
#include "AICharacter.generated.h"

class UBehaviorTree;
class UWidgetComponent;
UCLASS()
class DEMOTPS_API AAICharacter : public ABaseCharacter
{
	GENERATED_BODY()
public:
	AAICharacter(const FObjectInitializer& Object);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "AI")
	UBehaviorTree*  BehaviorTreeAsset;

	//TODO: Сделать обновление не на каждый тик
	virtual void Tick(float DeltaSeconds) override;
	
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Components")
	UWidgetComponent* HealthWidgetComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "AI")
	float HealthVisibilityDistance = 1000.f;
	
	virtual void BeginPlay() override;
	virtual void OnHealthChange(float health, float HealthDelta) override;
	virtual void OnDeath() override;
private:
	void UpdateHealthWidgetVisibility();
	
};
