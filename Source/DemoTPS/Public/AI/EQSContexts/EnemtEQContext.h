// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryContext.h"
#include "EnemtEQContext.generated.h"

/**
 * 
 */
UCLASS()
class DEMOTPS_API UEnemtEQContext : public UEnvQueryContext
{
	GENERATED_BODY()
public:
	virtual void ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const override;

protected:
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AI")
		FName EnemyActorKeyName = "EnemyActor";
};
