// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CoverComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DEMOTPS_API UCoverComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCoverComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void SetCanTrace(bool trace) { bCanTrace = trace; }
	void SetWallDetected(bool wallDetected) { bWallDetected = wallDetected; }
	void SetFixRotRealTime(bool fixRotRealTime) {bFixRotRealTime = fixRotRealTime;}

	bool GetCanTrace() const { return bCanTrace; }
	bool GetWallDetected() const { return bWallDetected; }
	bool GetFixRotRealTime() const { return bFixRotRealTime;}
	FVector GetCoverNormal() const { return CoverNormal; }
	FHitResult GetHitCover() const { return HitCover; }

	void TakeCover();
	void SetParametrsForCover(bool OrientRotMovement, bool RotationYaw, float speed);
	bool DetectMovementInCover(float direction);
	void QuitCover();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	bool bCanTrace = false;
	bool bWallDetected;
	bool bFixRotRealTime;
	

	float FixCharacterRotation;

	UPROPERTY()
	FVector CoverNormal;

private:
	FLatentActionInfo LatentInfo;
	TArray<TEnumAsByte<EObjectTypeQuery> > ObjectTypes = { EObjectTypeQuery::ObjectTypeQuery9 };
	const TArray< AActor* > ActorsToIgnore = {};
	FHitResult HitCover = FHitResult();
	float TraceRadius = 61.f;
	float CapsuleTraceHalfHeight = 30.f;
	
	
};
