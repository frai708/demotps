// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DemoTPSCoreTypes.h"
#include "WeaponComponent.generated.h"

struct FInputActionValue;
class ABaseWeapon;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DEMOTPS_API UWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UWeaponComponent();

	virtual void StartFire();
	virtual void NextWeapon();



	void StopFire();
	void Reload();
	void Zoom(bool Enabled);
	void Zoom(const FInputActionValue& Value);
	bool getIsZooming() const;
	float GetCurrentWeaponSensetivity() const; 

	bool TryToAddAmmo(TSubclassOf<ABaseWeapon> WeaponType, int32 ClipsAmount);
	bool TryToAddWeapon(TSubclassOf<ABaseWeapon> WeaponType);
	bool TryToReloadAllWeapon(int32 ClipsAmount);
	bool NeedAmmo(TSubclassOf<ABaseWeapon> WeaponType);

	bool GetWeaponUIData(FWeaponUIData& UIData) const;
	bool GetWeaponAmmoData(FAmmoData& AmmoData) const;

protected:

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(EditDefaultsOnly, Category="Weapon")
	TArray<FWeaponData> WeaponData;

	UPROPERTY(EditDefaultsOnly, Category="Animation")
	UAnimMontage* EquipAnimMontage;
	
	UPROPERTY(EditDefaultsOnly, Category="Weapon")
	FName WeaponEquipSocketName = "WeaponSoket";

	UPROPERTY(EditDefaultsOnly, Category="Weapon")
	FName WeaponArmourySocketName = "ArmorySocket";

	UPROPERTY()
	ABaseWeapon* CurrentWeapon = nullptr;
	
	UPROPERTY()
	TArray<ABaseWeapon*> Weapons;

	int32 CurrentWeaponIndex = 0;

	bool CanFire() const;
    bool CanEquip() const;
	void EquipWeapon(int32 WeaponIndex);

private:

	UPROPERTY()
	UAnimMontage* CurrentReloadAnimMontage = nullptr;

	bool bEquipAnimInProgress = false;
	bool bRealoadAnimProgress = false;
	bool bIsZooming = false;

	void SpawnWeapons();
	void AttachWeaponToSocket(ABaseWeapon* Weapon, USceneComponent* SceneComponent, const FName& SocketName);	
	void PlayAnimMontage(UAnimMontage* Animation);
	void InitAnimaions();
	void OnEquipFinshed(USkeletalMeshComponent* MeshComponent);
	void OnReloadFinished(USkeletalMeshComponent* MeshComponent);

	void OnEmptyClip(ABaseWeapon* AmmoEmptyWeapon);
	void ChangeClip();

	bool CanReload() const;

};
