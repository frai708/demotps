// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DemoTPSCoreTypes.h"
#include "HelthComponent.generated.h"

class UPhysicalMaterial;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DEMOTPS_API UHelthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	//Blueprint Methods
	UFUNCTION(BlueprintCallable, category = "Health")
	bool IsDead() const {return FMath::IsNearlyZero(Health);}

	UFUNCTION(BlueprintCallable, category = "Health")
	float GetHealthProcent() const {return Health / MaxHealth;}
//-----------------------

	//Delegats
	FOnDearh OnDeath;
	FOnHealthChanged OnHealthChanged;
//--------------------------
	
	UHelthComponent();

	//Getters
	float GetHealth() const {return Health;};
//-------------------------
	bool TryToAddHealth(float HealthAmount);
	bool IsHealFull() const;

protected:
	//Properties
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite,Category = "Health", meta = (ClampMin = "0.0", ClampMax = "100.0"))
	float MaxHealth = 100;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal")
	bool bAutoHeal = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Heal", meta =(EditCondition = "bAutoHeal"))
	float HealUpdateTime = 1.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Heal")
	TSubclassOf<UCameraShakeBase> CameraShake;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Health")
	TMap<UPhysicalMaterial*, float> DamageModifiers;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Heal", meta =(EditCondition = "bAutoHeal"))
	float HealDelay = 3.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Heal", meta =(EditCondition = "bAutoHeal"))
	float HealModifier = 5.f;
//---------------
	//Virtual functions
	virtual void BeginPlay() override;
//------------------
private:
	//Variables
	float Health  = 0.f;
	FTimerHandle HealTimerHandel;
//---------------

	//Methods
	UFUNCTION()
	void OnTakeAnyDamageHandle(AActor* DamagedActor,
							   float Damage,
							   const class UDamageType* DamageType,
							   class AController* InstigatedBy,
							   AActor* DamageCauser );

	UFUNCTION()
	void OnTakePointDamageHandle(AActor* DamagedActor,
						   float Damage,
						   class AController* InstigatedBy,
						   FVector HitLocation,
						   class UPrimitiveComponent* FHitComponent,
						   FName BoneName,
						   FVector ShotFromDirection,
						   const class UDamageType* DamageType,
						   AActor* DamageCauser );
	UFUNCTION()
	void OnTakeRadialDamageHandle (AActor* DamagedActor,
							 float Damage,
							 const class UDamageType* DamageType,
							 FVector Origin,
							 const FHitResult& HitInfo,
							 class AController* InstigatedBy,
							 AActor* DamageCauser );
	void HealUpdate();
	void SetHealth(float NewHealth);
	void PlayCameraShake();

	void Killed(AController* KillerController);
	void ApplyDamage(float Damage, AController* InstigatedBy);
	float GetPointDamageModifier(AActor* DamageActor, const FName& BoneName);

	void ReportDamageEvent(float Damage, AController* InstigateBy);
//------------------	
};
