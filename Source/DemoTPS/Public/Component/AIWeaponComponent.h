// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "Component/WeaponComponent.h"
#include "AIWeaponComponent.generated.h"

UCLASS()
class DEMOTPS_API UAIWeaponComponent : public UWeaponComponent
{
	GENERATED_BODY()

public:
	virtual void StartFire() override;
	virtual void NextWeapon() override;
};
