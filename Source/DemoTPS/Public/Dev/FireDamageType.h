// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "FireDamageType.generated.h"

UCLASS()
class DEMOTPS_API UFireDamageType : public UDamageType
{
	GENERATED_BODY()
};
