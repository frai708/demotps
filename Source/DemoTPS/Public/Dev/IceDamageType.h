// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "IceDamageType.generated.h"

UCLASS()
class DEMOTPS_API UIceDamageType : public UDamageType
{
	GENERATED_BODY()
	
};
