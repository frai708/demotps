// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "DemoTPSCoreTypes.h"
#include "GameHUD.generated.h"

class UBaseWidget;
UCLASS()
class DEMOTPS_API AGameHUD : public AHUD
{
	GENERATED_BODY()

public:
	virtual void DrawHUD() override;
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> PlayerHUDWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> PauseWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> GameOverWidgetClass;
	
	
	virtual void BeginPlay() override;
	
private:

	UPROPERTY()
	TMap<EMatchState, UBaseWidget*> GameWidgets;

	UPROPERTY()
		UBaseWidget* CurrentWidget = nullptr;
	
	void DrawCrossHair();
	void OnMatchStateChanged(EMatchState State);
};
