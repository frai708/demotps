// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "BaseWidget.h"
#include "PauseWidget.generated.h"

class UButton;

UCLASS()
class DEMOTPS_API UPauseWidget : public UBaseWidget
{
	GENERATED_BODY()
public:
	virtual void NativeOnInitialized() override;

protected:
	UPROPERTY(meta = (BindWidget))
	UButton* ClearPauseButton;

private:
	UFUNCTION()
	void OnClearPause();
	
};
