// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameDataWidget.generated.h"

class ATPSPlayerState;
class AMainGameMode;
UCLASS()
class DEMOTPS_API UGameDataWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	

	UFUNCTION(BlueprintCallable, Category="UI")
	int32 GetCurrentRoundNum() const;

	UFUNCTION(BlueprintCallable, Category="UI")
	int32 GetTotalRoundNum() const;

	UFUNCTION(BlueprintCallable, Category="UI")
	int32 GetRoundSeconsRemaining() const;

private:
	AMainGameMode* GetMainGameMode() const;
	ATPSPlayerState* GetTPSPlayerState() const;
};
