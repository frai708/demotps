// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "BaseWidget.h"
#include "DemoTPSCoreTypes.h"
#include "GameOverWidget.generated.h"

class UVerticalBox;
class UButton;

UCLASS()
class DEMOTPS_API UGameOverWidget : public UBaseWidget
{
	GENERATED_BODY()


protected:
	UPROPERTY(meta = (BindWidget))
	UVerticalBox* PlayerStatBox;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> PlayerStatRowWidgetClass;

	UPROPERTY(meta = (BindWidget))
	UButton* ResetLevelButton;

	virtual void NativeOnInitialized() override;
	
private:
	void OnMatchStateChanged(EMatchState State);
	void UpdatePlayerState();

	UFUNCTION()
		void OnResetLevel();
};
