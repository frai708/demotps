﻿#pragma once
#include "DemoTPSCoreTypes.generated.h"

class ABaseWeapon;

//Health
DECLARE_MULTICAST_DELEGATE(FOnDearh)
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnHealthChanged, float, float)

//Weapon
DECLARE_MULTICAST_DELEGATE_OneParam(FOnClipEmptySignature, ABaseWeapon*)

USTRUCT(BlueprintType)
struct FAmmoData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Weapon")
	int32 Bullets;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Weapon", meta = (EditCondition = "!bInfinite"))
	int32 Clips;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Weapon")
	bool bInfinite;
};

USTRUCT(BlueprintType)
struct FWeaponData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite,Category="Weapon")
	UAnimMontage* ReloadAnimMontage; 
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite,Category="Weapon")
	TSubclassOf<ABaseWeapon> WeaponClass;
};

USTRUCT(BlueprintType)
struct FWeaponUIData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="UI")
	UTexture2D* MainItem;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="UI")
	UTexture2D* CrossHairIcon;
};

//Decals
class UNiagaraSystem;
class USoundCue;
USTRUCT(BlueprintType)
struct FDecalData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="VFX")
	FVector Size = FVector(10.f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="VFX")
	float FadeOutTime = 0.7f; 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="VFX")
	float LifeTime = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="VFX")
	UMaterialInterface* Material;
	
};

USTRUCT(BlueprintType)
struct FImpactData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="VFX")
	FDecalData DecalData;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="VFX")
	UNiagaraSystem* NiagaraEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="VFX")
	USoundCue* Sound;
};

USTRUCT(BlueprintType)
struct FGameData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Game", meta = (ClampMin ="1", ClampMax = "100"))
	int32 PlayersNum = 2;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Game", meta = (ClampMin ="1", ClampMax = "10"))
	int32 RoundNum =4;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Game", meta = (ClampMin ="3", ClampMax = "300"))
	int32 RoundTime = 10; //in second
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FLinearColor DefaultTeamOnlyColor = FLinearColor::Red;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<FLinearColor> TeamColors;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Game", meta = (ClampMin ="3", ClampMax = "20"))
	int32 RespawnTime = 2;
};

UENUM(BlueprintType)
enum class EMatchState: uint8
{
	WaitingToStart = 0,
	InProgress,
	Pause,
	GameOver
};

DECLARE_MULTICAST_DELEGATE_OneParam(FOnMatchStateChangeSignature, EMatchState);

USTRUCT(BlueprintType)
struct FLevelData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Game")
	FName LevelName = NAME_None;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category= "Game")
	FName LevelDisplayName = NAME_None;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category= "Game")
	UTexture2D* LevelThumb;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game")
	int PlayersCount = 4;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game")
	int RoundCount = 2;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game")
	int RoundTime = 60;
};

DECLARE_MULTICAST_DELEGATE_OneParam(FOnLevelSelectedSignature, const FLevelData&);

