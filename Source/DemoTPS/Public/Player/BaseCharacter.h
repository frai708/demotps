// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseCharacter.generated.h"

class ABaseWeapon;
class UHelthComponent;
class UWeaponComponent;
class USoundCue;


UCLASS()
class DEMOTPS_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:

	ABaseCharacter(const FObjectInitializer& ObjectInitializer);

	virtual void Tick(float DeltaTime) override;
	virtual void TurnOff() override;
	virtual void Reset() override;
	void SetPlayerColor(const FLinearColor& Color);

	UFUNCTION(BlueprintCallable, Category="Movement")
	virtual bool IsRunning() const ;
	
	UFUNCTION(BlueprintCallable, Category="Movement")
	float GetMovementDirection() const;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Components")
	UHelthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Component")
	UWeaponComponent* WeaponComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Sound")
	USoundCue* DeathSound;
	
	UPROPERTY(VisibleAnywhere, Category="Game")
	FName MaterialColorName = "Paint Color";


	
protected:
	//Properties

	UPROPERTY(EditDefaultsOnly, Category="Animation")
	UAnimMontage* DeathAnimMontage;

	UPROPERTY(EditDefaultsOnly, Category="Damage")
	FVector2D LandedDamageVelocity = FVector2D(900.f,1200.f);

	UPROPERTY(EditDefaultsOnly, Category="Damage")
	FVector2D LandedDamage = FVector2D(10.f, 100.f);
//------------
	//Virtual methods
	virtual void BeginPlay() override;
	virtual void OnDeath();
	virtual void OnHealthChange(float health, float HealthDelta);
//--------------	
private:


	
	UFUNCTION()
   	void OnGroundLanded(const FHitResult& hit);

//-------------------	
};
