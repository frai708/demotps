// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "TPSPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class DEMOTPS_API ATPSPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	void SetTeamID(int32 ID) {TeamID = ID;}
	int32 GetTeamID() const {return TeamID;}

	void SetTeamColor(const FLinearColor& Color) {TeamColor = Color;}
	FLinearColor GetTeamColor() const {return TeamColor;}

	void AddKill() {++KillsSum;}
	int32 GetKillsNum() const {return KillsSum;}
	
	void AddDeath() {++DeathNum;}
	int32 GetDeathNum() const {return DeathNum;}

	void LogInfo();

private:
	int32 TeamID;
	FLinearColor TeamColor;

	int32 KillsSum = 0;
	int32 DeathNum = 0;
};
