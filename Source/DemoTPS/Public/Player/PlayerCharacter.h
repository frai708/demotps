// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"
#include "Player/BaseCharacter.h"
#include "InputActionCatalog.h"
#include "PlayerCharacter.generated.h"

struct FInputActionValue;
class UInputAction;
class USpringArmComponent;
class UCameraComponent;
class USphereComponent;
class UInputMappingContext;
class UCoverComponent;


UCLASS()
class DEMOTPS_API APlayerCharacter : public ABaseCharacter
{
	GENERATED_BODY()
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components", meta=(AllowPrivateAccess = "true"))
	UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=camera, meta=(AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=camera, meta=(AllowPrivateAccess = "true"))
	USphereComponent* CameraCollisionComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Component, meta = (AllowPrivateAccess = "true"))
	UCoverComponent* CoverComponent;


public:
	APlayerCharacter(const FObjectInitializer& ObjectInitializer);
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


protected:

	UPROPERTY(BlueprintReadOnly, category = "Movement", meta = (AllowPrivateAccess = "true"))
	float Direction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input")
	UInputMappingContext* PlayerMappingContext;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input")
	FInputActionCatalog InputCatalog;
	
	virtual void BeginPlay() override;
	virtual bool IsRunning() const override;
	virtual void OnDeath();

private:
	

	bool bWantsToRun;
	bool bIsMovingFarward;
	bool bInCover = false;
	bool bShootInCover = true;

	void SetInCover() { bInCover = true; }
	
	void MoveForward(const FInputActionValue& Value);
	void CrouchAction();
	void UnCrouchAction();
	void Look(const FInputActionValue& Value);
	void OnStartRunning();
	void OnStopRunning();
	void SwitchCover();
	void Shoot();

	UFUNCTION()
	void OnCameraCollisionBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnCameraCollisionEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void CheckCameraOverlap();
};
