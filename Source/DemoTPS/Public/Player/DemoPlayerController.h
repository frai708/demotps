// Demo project for portfolio. Lev Lobachev 

#pragma once

#include "CoreMinimal.h"
#include "DemoTPSCoreTypes.h"
#include "GameFramework/PlayerController.h"
#include "DemoPlayerController.generated.h"

class URespawnComponent;
UCLASS()
class DEMOTPS_API ADemoPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	ADemoPlayerController();
	
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	URespawnComponent* RespawnComponent;

	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* InPawn) override;
	virtual void SetupInputComponent() override;

private:
	void OnPauseGame();
	void OnMatchStateChanged(EMatchState State);
	void OnMuteSound();
	
};
