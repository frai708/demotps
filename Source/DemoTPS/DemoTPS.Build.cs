// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class DemoTPS : ModuleRules
{
	public DemoTPS(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core",
																	"CoreUObject",
																	"Engine",
																	"InputCore",
																	"Niagara",
																	"PhysicsCore",
																	"GameplayTasks",
																	"NavigationSystem",
																	"EnhancedInput"
		});

		PrivateDependencyModuleNames.AddRange(new string[] {  });

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");
		
		PublicIncludePaths.AddRange(new string[] {"DemoTPS/Component",
															"DemoTPS/Public/Player", 
															"DemoTPS/Public/Component", 
															"DemoTPS/Public/Dev", 
															"DemoTPS/Public/Weapon",
															"DemoTPS/Public/UI",
															"DemoTPS/Public/Menu/UI",
															"DemoTPS/Public/Menu",
															"DemoTPS/Public/Animations",
															"DemoTPS/Public/Pickups",
															"DemoTPS/Public/Weapon/Components",
															"DemoTPS/Public/AI",
															"DemoTPS/Public/AI/Tasks",
															"DemoTPS/Public/AI/Services",
															"DemoTPS/Public/AI/EQSContexts",
															"DemoTPS/Public/AI/Decorators",
															"DemoTPS/Public/Sound"
		});

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
