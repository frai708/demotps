// Demo project for portfolio. Lev Lobachev 


#include "Menu/MenuController.h"
#include "TPSGameInstance.h"

void AMenuController::BeginPlay()
{
	Super::BeginPlay();

	SetInputMode(FInputModeUIOnly());
	bShowMouseCursor = true;

	GetWorld()->GetGameInstance<UTPSGameInstance>()->TestString = "Menu Level";
}
