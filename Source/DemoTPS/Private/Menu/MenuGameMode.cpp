// Demo project for portfolio. Lev Lobachev 


#include "Menu/MenuGameMode.h"
#include "Menu/MenuController.h"
#include "Menu/UI/MenuHUD.h"

AMenuGameMode::AMenuGameMode()
{
	PlayerControllerClass = AMenuController::StaticClass();
	HUDClass = AMenuHUD::StaticClass();
}
