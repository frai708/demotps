// Demo project for portfolio. Lev Lobachev 


#include "Menu/UI/MenuWidget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "TPSGameInstance.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/HorizontalBox.h"
#include "Components/VerticalBox.h"
#include "Menu/UI/LevelItemWidget.h"
#include "Menu/UI/LevelSettingsWidget.h"
#include "DemoTPS/MainGameMode.h"
#include "Sound/SoundCue.h"

DEFINE_LOG_CATEGORY_STATIC(LogMenuWidget, All, All);

void UMenuWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if(StartGameButton)
	{
		StartGameButton->OnClicked.AddDynamic(this, &ThisClass::OnStartGame);
	}

	if(QuitGameButton)
	{
		QuitGameButton->OnClicked.AddDynamic(this, &ThisClass::OnQuitGame);
	}

	if (ShowLevelButton)
	{
		ShowLevelButton->OnClicked.AddDynamic(this, &ThisClass::OnShowItem);
	}
	InitLevelItems();

}

void UMenuWidget::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
	if(Animation != HideAnimation) return;
	const auto TPSGameInstance = GetTPSGameInstance();
	if(!TPSGameInstance) return;

	UGameplayStatics::OpenLevel(this, TPSGameInstance->GetStartupLevel().LevelName);
}


void UMenuWidget::OnStartGame()
{
	PlayAnimation(HideAnimation);
	UGameplayStatics::PlaySound2D(GetWorld(), StartGameSound);

	const auto TPSGameInstance = GetTPSGameInstance();
	if (!TPSGameInstance) return;

	auto TempData = TPSGameInstance->GetStartupLevel();

	if (PlayerCount)
		TempData.PlayersCount = PlayerCount->GetCount();
	if (RoundCount)
		TempData.RoundCount = RoundCount->GetCount();
	if (TimeCount)
		TempData.RoundTime = TimeCount->GetCount();

	TPSGameInstance->SetStartupLevel(TempData);
}

void UMenuWidget::OnShowItem()
{
	if (!LevelShowBox->IsVisible() && !LevelSettingsBox->IsVisible())
	{
		LevelShowBox->SetVisibility(ESlateVisibility::Visible);
		LevelSettingsBox->SetVisibility(ESlateVisibility::Visible);
	}
	else
	{
		LevelShowBox->SetVisibility(ESlateVisibility::Collapsed);
		LevelSettingsBox->SetVisibility(ESlateVisibility::Collapsed);
	}

}

void UMenuWidget::OnQuitGame()
{
	UKismetSystemLibrary::QuitGame(this, GetOwningPlayer(), EQuitPreference::Quit, true);
}

void UMenuWidget::InitLevelItems()
{
	const auto TPSGameInstance = GetTPSGameInstance();
	if(!TPSGameInstance) return;

	checkf(TPSGameInstance->GetLevelsData().Num() !=0, TEXT("Levels data must not empty"));
	if(!LevelItemBox) return;

	LevelItemBox->ClearChildren();

	for(auto LevelData : TPSGameInstance->GetLevelsData())
	{
		const auto LevelItemWidget = CreateWidget<ULevelItemWidget>(GetWorld(), LevelItemWidgetClass);
		if(!LevelItemWidget) continue;
		LevelItemWidget->SetLevelData(LevelData);
		LevelItemWidget->OnLevelSelected.AddUObject(this, &ThisClass::OnLevelSelected);

		LevelItemBox->AddChild(LevelItemWidget);
		LevelItemWidgets.Add(LevelItemWidget);
	}

	if(TPSGameInstance->GetStartupLevel().LevelName.IsNone())
	{
		OnLevelSelected(TPSGameInstance->GetLevelsData()[0]);
	}
	else
	{
		OnLevelSelected(TPSGameInstance->GetStartupLevel());
	}
}

void UMenuWidget::OnLevelSelected(const FLevelData& Data)
{

	const auto TPSGameInstance = GetTPSGameInstance();
	if (!TPSGameInstance) return;

	TPSGameInstance->SetStartupLevel(Data);
	for (auto LevelItemWidget : LevelItemWidgets)
	{
		if (LevelItemWidget)
		{
			const auto IsSelected = Data.LevelDisplayName == LevelItemWidget->GetLevelData().LevelDisplayName;
			LevelItemWidget->SetSelected(IsSelected);
		}
	}
}

UTPSGameInstance* UMenuWidget::GetTPSGameInstance() const
{
	if(!GetWorld()) return nullptr;

	return GetWorld()->GetGameInstance<UTPSGameInstance>();
}
