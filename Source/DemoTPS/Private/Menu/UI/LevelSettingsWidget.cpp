// Demo project for portfolio. Lev Lobachev 


#include "Menu/UI/LevelSettingsWidget.h"
#include "Components/Slider.h"
#include "Components/TextBlock.h"

int ULevelSettingsWidget::GetCount() const
{

	return S_Amount->GetValue();
}

void ULevelSettingsWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (S_Amount)
	{
		S_Amount->SetMinValue(MinValue);
		S_Amount->SetMaxValue(MaxValue);
		S_Amount->SetValue(SliderValue);
		S_Amount->OnValueChanged.AddDynamic(this, &ThisClass::OnChanged);
	}

	if (TB_Count)
	{
		TB_Count->SetText(FText::AsNumber(S_Amount->GetValue()));
	}

	if (TB_SettingInformation)
	{
		TB_SettingInformation->SetText(PlayerAmountText);
	}

}

void ULevelSettingsWidget::OnChanged(float Value)
{
	if (TB_Count)
	{
		int IValue = Value;
		TB_Count->SetText(FText::AsNumber(IValue));
	}
}
