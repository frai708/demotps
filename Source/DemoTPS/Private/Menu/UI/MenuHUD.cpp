// Demo project for portfolio. Lev Lobachev 


#include "Menu/UI/MenuHUD.h"
#include "UI/BaseWidget.h"
#include "Blueprint/UserWidget.h"

void AMenuHUD::BeginPlay()
{
	Super::BeginPlay();

	if(MenuWidgetClass)
	{
		const auto MenuWidget =CreateWidget<UBaseWidget>(GetWorld(), MenuWidgetClass);
		if(MenuWidget)
		{
			MenuWidget->AddToViewport();
			MenuWidget->Show();
		}
	}
}
