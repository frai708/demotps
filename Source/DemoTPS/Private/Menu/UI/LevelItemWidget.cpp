// Demo project for portfolio. Lev Lobachev 


#include "Menu/UI/LevelItemWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"

void ULevelItemWidget::SetLevelData(const FLevelData& Data)
{
	LevelData = Data;

	if(LevelNameTextBlock)
	{
		LevelNameTextBlock->SetText(FText::FromName(Data.LevelDisplayName));
	}

	if(LevelImage)
	{
		LevelImage->SetBrushFromTexture(Data.LevelThumb);
	}
}

void ULevelItemWidget::SetSelected(bool IsSelected)
{
	if(LevelImage)
	{
		LevelImage->SetColorAndOpacity(IsSelected ? FLinearColor::Red : FLinearColor::White);
	}
}

void ULevelItemWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if(LevelSelectButton)
	{
		LevelSelectButton->OnClicked.AddDynamic(this, &ThisClass::OnLevelItemClicked);
		LevelSelectButton->OnHovered.AddDynamic(this, &ThisClass::OnLevelItemHovered);
		LevelSelectButton->OnUnhovered.AddDynamic(this, &ThisClass::OnLevelItemUnHovered);
	}
}

void ULevelItemWidget::OnLevelItemClicked()
{
	OnLevelSelected.Broadcast(LevelData);
}

void ULevelItemWidget::OnLevelItemHovered()
{
	if(FrameImage)
	{
		FrameImage->SetVisibility(ESlateVisibility::Visible);
	}
}

void ULevelItemWidget::OnLevelItemUnHovered()
{
	if(FrameImage)
	{
		FrameImage->SetVisibility(ESlateVisibility::Hidden);
	}
}
