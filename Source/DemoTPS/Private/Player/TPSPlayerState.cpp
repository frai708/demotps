// Demo project for portfolio. Lev Lobachev 


#include "Player/TPSPlayerState.h"

DEFINE_LOG_CATEGORY_STATIC(LogPlayerState, All, All);

void ATPSPlayerState::LogInfo()
{
	UE_LOG(LogPlayerState, Display, TEXT("TeamID: %i, Kills: %i, Death: %i"), TeamID, KillsSum, DeathNum);
}
