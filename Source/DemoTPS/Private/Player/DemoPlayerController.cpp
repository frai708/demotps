// Demo project for portfolio. Lev Lobachev 

#include "DemoPlayerController.h"
#include "DemoTPS/MainGameMode.h"
#include "TPSGameInstance.h"
#include "RespawnComponent.h"

ADemoPlayerController::ADemoPlayerController()
{
	RespawnComponent = CreateDefaultSubobject<URespawnComponent>(TEXT("RespawnComponent"));
}

void ADemoPlayerController::BeginPlay()
{
	Super::BeginPlay();
	
	if(GetWorld())
	{
		if(const auto GameMode = Cast<AMainGameMode>(GetWorld()->GetAuthGameMode()))
		{
			GameMode->OnMatchChangeState.AddUObject(this, &ThisClass::OnMatchStateChanged);
		}
	}
}

void ADemoPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	OnNewPawn.Broadcast(InPawn);
}

void ADemoPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	if(!InputComponent) return;

	InputComponent->BindAction("PauseGame", IE_Pressed, this, &ThisClass::OnPauseGame);
	InputComponent->BindAction("Mute", IE_Pressed, this, &ThisClass::OnMuteSound);
}

void ADemoPlayerController::OnPauseGame()
{
	if(!GetWorld() || !GetWorld()->GetAuthGameMode()) return;

	GetWorld()->GetAuthGameMode()->SetPause(this);
}

void ADemoPlayerController::OnMatchStateChanged(EMatchState State)
{
	if(State == EMatchState::InProgress)
	{
		SetInputMode(FInputModeGameOnly());
		bShowMouseCursor = false;
	}
	else
	{
		SetInputMode(FInputModeUIOnly());
		bShowMouseCursor = true;
	}
}

void ADemoPlayerController::OnMuteSound()
{
	if(!GetWorld()) return;
	const auto TPSGameInstance = GetWorld()->GetGameInstance<UTPSGameInstance>();
	if(!TPSGameInstance) return;

	TPSGameInstance->ToggleVolume();
}
