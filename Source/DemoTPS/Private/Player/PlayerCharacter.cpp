// Demo project for portfolio. Lev Lobachev 


#include "Player/PlayerCharacter.h"
#include "DemoPlayerController.h"
#include "WeaponComponent.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/SphereComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "CoverComponent.h"
#include "InputActionValue.h"
#include "DemoTPS/Component/TPSCharacterMovementComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogPlayerCharacter, All,All)

APlayerCharacter::APlayerCharacter(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400;
	CameraBoom->bUsePawnControlRotation = true;
	
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->SetupAttachment(CameraBoom);
	CameraComponent->bUsePawnControlRotation = false;

	CameraCollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("CameraCollisionComponent"));
	CameraCollisionComponent->SetupAttachment(CameraComponent);
	CameraCollisionComponent->SetSphereRadius(10.f);
	CameraCollisionComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

	CoverComponent = CreateDefaultSubobject<UCoverComponent>(TEXT("CoverComponent"));
}

void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	check(CameraCollisionComponent);
	check(CoverComponent);

	CameraCollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &APlayerCharacter::OnCameraCollisionBeginOverlap);
	CameraCollisionComponent->OnComponentEndOverlap.AddDynamic(this, &APlayerCharacter::OnCameraCollisionEndOverlap);

	if(const auto PlayerController = Cast<ADemoPlayerController>(GetController()))
	{
		if(const auto Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(PlayerMappingContext, 0);
		}
	}
}

void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);
	check(WeaponComponent);

	if(const auto EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EnhancedInputComponent->BindAction(InputCatalog.MoveAction, ETriggerEvent::Triggered, this, &APlayerCharacter::MoveForward);
		EnhancedInputComponent->BindAction(InputCatalog.LookAction, ETriggerEvent::Triggered, this, &APlayerCharacter::Look);
		
		EnhancedInputComponent->BindAction(InputCatalog.JumpAction, ETriggerEvent::Triggered, this, &ABaseCharacter::Jump);
		EnhancedInputComponent->BindAction(InputCatalog.JumpAction, ETriggerEvent::Completed, this, &ABaseCharacter::StopJumping);
		
		EnhancedInputComponent->BindAction(InputCatalog.RunAction, ETriggerEvent::Triggered, this, &APlayerCharacter::OnStartRunning);
		EnhancedInputComponent->BindAction(InputCatalog.RunAction, ETriggerEvent::Completed, this, &APlayerCharacter::OnStopRunning);

		//EnhancedInputComponent->BindAction(InputCatalog.CrouchAction, ETriggerEvent::Triggered, this, &APlayerCharacter::CrouchAction);
		//EnhancedInputComponent->BindAction(InputCatalog.CrouchAction, ETriggerEvent::Completed, this, &APlayerCharacter::UnCrouchAction);
		
		//EnhancedInputComponent->BindAction(InputCatalog.FireAction, ETriggerEvent::Triggered, this,  &ThisClass::Shoot);
		//EnhancedInputComponent->BindAction(InputCatalog.FireAction, ETriggerEvent::Canceled, WeaponComponent,  &UWeaponComponent::StopFire);
		
		EnhancedInputComponent->BindAction(InputCatalog.ZoomAction, ETriggerEvent::Triggered, WeaponComponent,  &UWeaponComponent::Zoom);
		EnhancedInputComponent->BindAction(InputCatalog.ZoomAction, ETriggerEvent::Completed, WeaponComponent,  &UWeaponComponent::Zoom);
		
		EnhancedInputComponent->BindAction(InputCatalog.ReloadAction, ETriggerEvent::Triggered, WeaponComponent,  &UWeaponComponent::Reload);
		EnhancedInputComponent->BindAction(InputCatalog.NextWeapon, ETriggerEvent::Completed, WeaponComponent,  &UWeaponComponent::NextWeapon);
	}
//	PlayerInputComponent->BindAction("Cover", IE_Pressed, this, &ThisClass::SwitchCover);
PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ThisClass::Shoot);
PlayerInputComponent->BindAction("Fire", IE_Released, WeaponComponent,  &UWeaponComponent::StopFire);
	
	
}

void APlayerCharacter::OnDeath()
{
	Super::OnDeath();
	if(Controller)
	{
		Controller->ChangeState(NAME_Spectating);
	}
}

bool APlayerCharacter::IsRunning() const
{
	return bWantsToRun && bIsMovingFarward && !GetVelocity().IsZero() && !bInCover;
}

void APlayerCharacter::MoveForward(const FInputActionValue& Value)
{
	FVector2D MovementValue = Value.Get<FVector2d>();

	bIsMovingFarward = MovementValue.Y > 0 && MovementValue.X == 0 && !bInCover;
	if (Controller)
	{
		const auto Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0.f, Rotation.Yaw, 0.f);
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		Direction = MovementValue.X;
		if (bInCover)
		{
			FHitResult hit;
			if (CoverComponent->DetectMovementInCover(MovementValue.X))
			{
				const auto NewDirection = UKismetMathLibrary::GetRightVector(UKismetMathLibrary::MakeRotFromX(CoverComponent->GetHitCover().Normal));
				AddMovementInput(NewDirection, -MovementValue.X);
				bShootInCover = false;
			}
			else
			{
				bShootInCover = true;
			}
		}
		else
		{
			AddMovementInput(ForwardDirection, MovementValue.Y);
			AddMovementInput(RightDirection, MovementValue.X);
		}
	}
}

void APlayerCharacter::CrouchAction()
{
	Crouch();
}

void APlayerCharacter::UnCrouchAction()
{
	UnCrouch();
}

void APlayerCharacter::Look(const FInputActionValue& Value)
{
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		if (WeaponComponent->getIsZooming())
		{
			auto DPIX = LookAxisVector.X * WeaponComponent->GetCurrentWeaponSensetivity() * UGameplayStatics::GetWorldDeltaSeconds(GetWorld());
			auto DPIY = LookAxisVector.Y * WeaponComponent->GetCurrentWeaponSensetivity() * UGameplayStatics::GetWorldDeltaSeconds(GetWorld());
			AddControllerYawInput(DPIX);
			AddControllerPitchInput(DPIY);
		
		}
		else
		{
			AddControllerYawInput(LookAxisVector.X);
			AddControllerPitchInput(LookAxisVector.Y);
		}
		
	}
}

void APlayerCharacter::OnStartRunning()
{
	bWantsToRun = true;
}

void APlayerCharacter::OnStopRunning()
{
	bWantsToRun = false;
}

void APlayerCharacter::SwitchCover()
{
	if (!bInCover)
	{
		GetCharacterMovement()->SetJumpAllowed(false);
		CoverComponent->TakeCover();
		bInCover = true;
		bShootInCover = false;
	}
	else
	{
		GetCharacterMovement()->SetJumpAllowed(true);
		CoverComponent->QuitCover();
		bInCover = false;
		bShootInCover = true;
	}
}

void APlayerCharacter::Shoot()
{
	if (bShootInCover)
		WeaponComponent->StartFire();
}

void APlayerCharacter::OnCameraCollisionBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	CheckCameraOverlap();
}

void APlayerCharacter::OnCameraCollisionEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	CheckCameraOverlap();
}

void APlayerCharacter::CheckCameraOverlap()
{
	const auto HideMesh = CameraCollisionComponent->IsOverlappingComponent(GetCapsuleComponent());
	GetMesh()->SetOwnerNoSee(HideMesh);
	TArray<USceneComponent*> MeshChildren;
	GetMesh()->GetChildrenComponents(true, MeshChildren);

	for(auto MeshChild : MeshChildren)
	{
		if(const auto MeshChildGeometry = Cast<UPrimitiveComponent>(MeshChild))
		{
			MeshChildGeometry->SetOwnerNoSee(HideMesh);
		}
	}
}
