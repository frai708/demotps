// Demo project for portfolio. Lev Lobachev 

#include "Pickups/AmmoPickup.h"
#include "Component/HelthComponent.h"
#include "Component/WeaponComponent.h"
#include "DemoTPSUtils.h"

DEFINE_LOG_CATEGORY_STATIC(LogAmmoPickup, All,All)

bool AAmmoPickup::GivePickupTo(APawn* PlayerPawn)
{
	const auto HealthComponent = FDemoTPSUtils::GetDemoPlayerComponent<UHelthComponent>(PlayerPawn);
	if(!HealthComponent || HealthComponent->IsDead()) return false;

	const auto WeaponComponent = FDemoTPSUtils::GetDemoPlayerComponent<UWeaponComponent>(PlayerPawn);
	if(!WeaponComponent) return false;

	//return WeaponComponent->TryToAddAmmo(WeaponType, ClipAmount);
	return WeaponComponent->TryToReloadAllWeapon(ClipAmount);
}
