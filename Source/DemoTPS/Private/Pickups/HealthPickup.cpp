// Demo project for portfolio. Lev Lobachev 

#include "HealthPickup.h"
#include "Component/HelthComponent.h"
#include "DemoTPSUtils.h"

DEFINE_LOG_CATEGORY_STATIC(LogHealthPickup, All,All)

bool AHealthPickup::GivePickupTo(APawn* PlayerPawn)
{
	const auto HealthComponent = FDemoTPSUtils::GetDemoPlayerComponent<UHelthComponent>(PlayerPawn);
	if(!HealthComponent) return false;
	
	UE_LOG(LogHealthPickup, Display, TEXT("Health was taken"));
	return HealthComponent->TryToAddHealth(HealthAmount);
}
