// Demo project for portfolio. Lev Lobachev 


#include "Pickups/WeaponPickup.h"
#include "Weapon/BaseWeapon.h"
#include "Component/WeaponComponent.h"
#include "DemoTPSUtils.h"



AWeaponPickup::AWeaponPickup()
{
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	WeaponMesh->SetupAttachment(GetRootComponent());
}

void AWeaponPickup::BeginPlay()
{
	Super::BeginPlay();

	WeaponMesh->SetSkeletalMeshAsset(WeaponType->GetDefaultObject<ABaseWeapon>()->GetWeaponMesh()->GetSkeletalMeshAsset());
	WeaponMesh->SetMaterial(0,WeaponType->GetDefaultObject<ABaseWeapon>()->GetWeaponMesh()->GetMaterial(0));
}

bool AWeaponPickup::GivePickupTo(APawn* PlayerPawn)
{
	const auto WeaponComponent = FDemoTPSUtils::GetDemoPlayerComponent<UWeaponComponent>(PlayerPawn);
	if (!WeaponComponent) return false;

	return WeaponComponent->TryToAddWeapon(WeaponType);
}
