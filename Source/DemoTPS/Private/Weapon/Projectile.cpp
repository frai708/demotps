// Demo project for portfolio. Lev Lobachev 


#include "Projectile.h"
#include "Components/SphereComponent.h"
#include "WeaponFXComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CollisionComponet = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionComponent"));
	CollisionComponet->InitSphereRadius(5.f);
	CollisionComponet->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComponet->SetCollisionResponseToAllChannels(ECR_Block);
	CollisionComponet->bReturnMaterialOnMove = true;
	SetRootComponent(CollisionComponet);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("MovementComponent"));
	ProjectileMovement->InitialSpeed = 2000.f;
	ProjectileMovement->ProjectileGravityScale = 0.f;

	WeaponFXComponent = CreateDefaultSubobject<UWeaponFXComponent>(TEXT("WeaponFXCoponent"));
	
}


void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	check(ProjectileMovement);
	check(CollisionComponet);
	check(WeaponFXComponent);

	ProjectileMovement->Velocity = ShotDirection * ProjectileMovement->InitialSpeed;
	CollisionComponet->IgnoreActorWhenMoving(GetOwner(), true);
	CollisionComponet->OnComponentHit.AddDynamic(this, &AProjectile::OnProjectileHit);
	SetLifeSpan(LifeSeconds);
}

AController* AProjectile::GetController() const
{
	const auto Pawn = Cast<APawn>(GetOwner());
	return Pawn ? Pawn->GetController() : nullptr;
}

void AProjectile::OnProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
                                  FVector NormalImpulse, const FHitResult& Hit)
{
	if(!GetWorld()) return;
	ProjectileMovement->StopMovementImmediately();
	//make damage
	UGameplayStatics::ApplyRadialDamage(GetWorld(),DamageAmount, GetActorLocation(), DamageRadius, UDamageType::StaticClass(), {},this, GetController(), DoFullDamage);
	
	DrawDebugSphere(GetWorld(), GetActorLocation(), DamageRadius, 24, FColor::Red, false, 5.f);
	WeaponFXComponent->PlayImpactFX(Hit);
	Destroy();
}


