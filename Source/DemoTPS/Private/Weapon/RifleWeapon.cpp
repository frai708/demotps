// Demo project for portfolio. Lev Lobachev 


#include "RifleWeapon.h"
#include "Components/WeaponFXComponent.h"
#include "Engine/DamageEvents.h"
#include "NiagaraComponent.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/AudioComponent.h"
#include "NiagaraFunctionLibrary.h"

ARifleWeapon::ARifleWeapon()
{
	WeaponFXComponent = CreateDefaultSubobject<UWeaponFXComponent>(TEXT("WeaponFXComponent"));
}

void ARifleWeapon::StartFire()
{
	if (bIsAuto)
	{
		InitFX();
		GetWorldTimerManager().SetTimer(ShotTimerHandle, this, &ARifleWeapon::MakeShot, TimeBetweenShots, true);
	}
	else
	{
		MakeShot();
		SpawnMuzzleFX();
		UGameplayStatics::SpawnSoundAttached(FireSound, WeaponMesh, MuzzleSocketName);
	}
}

void ARifleWeapon::StopFire()
{
		if(bIsAuto)
			GetWorldTimerManager().ClearTimer(ShotTimerHandle);
		
		SetFXActive(false);
}

void ARifleWeapon::Zoom(bool Enabled)
{
	const auto Controller = Cast<APlayerController>(GetController());
	if(!Controller || !Controller->PlayerCameraManager) return;

	if(Enabled)
	{
		DefaultCameraFOV = Controller->PlayerCameraManager->GetFOVAngle();
	}
	
	Controller->PlayerCameraManager->SetFOV(Enabled ? FOVZoomAngle : DefaultCameraFOV);
}

void ARifleWeapon::BeginPlay()
{
	Super::BeginPlay();
	check(WeaponFXComponent);

}

void ARifleWeapon::MakeShot()
{
	if(!GetWorld() || IsAmmoEmpty())
	{
		StopFire();
		return;
	}
	FVector TraceStart, TraceEnd;
	if(!GetTraceData(TraceStart,TraceEnd))
	{
		StopFire();
		return;
	}
	FHitResult HitResult;
	//FVector TraceFXEnd = TraceEnd;
	if (bIsShootGun)
	{
		for (int i = 0; i < BulletNumber; ++i)
		{
			float x = UKismetMathLibrary::RandomFloatInRange(ShotgunSpreed, -ShotgunSpreed);
			float y = UKismetMathLibrary::RandomFloatInRange(ShotgunSpreed, -ShotgunSpreed);
			float z = UKismetMathLibrary::RandomFloatInRange(ShotgunSpreed, -ShotgunSpreed);
			TraceEnd += FVector(x, y, z);
			MakeHit(HitResult, TraceStart, TraceEnd);
			FVector TraceFXEnd = TraceEnd;
			SingleShot(HitResult, TraceFXEnd);
			SpawnTraceFx(GetMuzzleWorldLocation(), TraceFXEnd);
		}
	}
	else
	{
		MakeHit(HitResult, TraceStart, TraceEnd);
		FVector TraceFXEnd = TraceEnd;
		SingleShot(HitResult, TraceFXEnd);
		SpawnTraceFx(GetMuzzleWorldLocation(), TraceFXEnd);
	}

	DecreaseAmmo();
}

bool ARifleWeapon::GetTraceData(FVector& TraceStart, FVector& TraceEnd) const
{
	FVector ViewLocation;
	FRotator ViewRotation;
	if(!GetPlayerViewPoint(ViewLocation, ViewRotation)) return false;
	
	TraceStart = ViewLocation;
	const auto HalfRad = FMath::DegreesToRadians(BulletSpreed);
	const FVector ShootDirection = FMath::VRandCone(ViewRotation.Vector(), HalfRad);
	TraceEnd = TraceStart+ShootDirection*TraceMaxDistance;
	return true;
}

void ARifleWeapon::SingleShot(FHitResult& Hit, FVector& End)
{
	if (Hit.bBlockingHit)
	{
		End = Hit.ImpactPoint;
		if (bIsShootGun)
		{
				MakeDamage(Hit);
				WeaponFXComponent->PlayImpactFX(Hit);
			
		}
		else
		{
			MakeDamage(Hit);
			WeaponFXComponent->PlayImpactFX(Hit);
		}
		//DrawDebugLine(GetWorld(), GetMuzzleWorldLocation(), HitResult.ImpactPoint, FColor::Red, false, 3.f,0,3.f);
		//DrawDebugSphere(GetWorld(),HitResult.ImpactPoint, 10.f,24,FColor::Red, false, 5.f);
		//UE_LOG(LogBaseWeapon, Display, TEXT("Bone %s"), *HitResult.BoneName.ToString());

	}
}

void ARifleWeapon::MakeDamage(const FHitResult& HitResult)
{
	const auto DamageActor = HitResult.GetActor();
	if(!DamageActor) return;

	FPointDamageEvent PointDamageEvent;
	PointDamageEvent.HitInfo = HitResult;
	DamageActor->TakeDamage(DamageAmount, PointDamageEvent, GetController(), this);

}

void ARifleWeapon::InitFX()
{
	if(!MuzzleFXComponent)
	{
		MuzzleFXComponent = SpawnMuzzleFX();
	}

	if(!FireAudioComponent)
	{
		FireAudioComponent = UGameplayStatics::SpawnSoundAttached(FireSound, WeaponMesh,MuzzleSocketName);
	}
	SetFXActive(true);
}

void ARifleWeapon::SetFXActive(bool IsActive)
{
	if(MuzzleFXComponent)
	{
		MuzzleFXComponent->SetPaused(!IsActive);
		MuzzleFXComponent->SetVisibility(IsActive, true);
	}

	if(FireAudioComponent)
	{
		IsActive ? FireAudioComponent->Play() : FireAudioComponent->Stop();
	}
}

void ARifleWeapon::SpawnTraceFx(const FVector& TraceStart, const FVector& TraceEnd)
{
	const auto TraceFXComponent = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), TraceFX, TraceStart);
	if(TraceFXComponent)
	{
		TraceFXComponent->SetNiagaraVariableVec3(TraceTargetName, TraceEnd);
	}
}

void ARifleWeapon::SpawnSingleShotEffects()
{

}



AController* ARifleWeapon::GetController() const
{
	const auto Pawn = Cast<APawn>(GetOwner());
	return Pawn ? Pawn->GetController() : nullptr;
}