// Demo project for portfolio. Lev Lobachev 

#include "WeaponFXComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Components/DecalComponent.h"
#include "Sound/SoundCue.h"

UWeaponFXComponent::UWeaponFXComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UWeaponFXComponent::PlayImpactFX(const FHitResult& hit)
{
	auto ImpactData = DefaultImpactData;
	if(hit.PhysMaterial.IsValid())
	{
		const auto PhysMat = hit.PhysMaterial.Get();
		if(ImpactDataMap.Contains(PhysMat))
		{
			ImpactData = ImpactDataMap[PhysMat];
		}
	}
	//niagara
	UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),
												   ImpactData.NiagaraEffect,
												   hit.ImpactPoint,
												   hit.ImpactNormal.Rotation());
	//decal
	auto DecalComponent = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), //
											ImpactData.DecalData.Material,
											ImpactData.DecalData.Size,
											hit.ImpactPoint,
											hit.ImpactNormal.Rotation());

	if(DecalComponent)
	{
		DecalComponent->SetFadeOut(ImpactData.DecalData.LifeTime, ImpactData.DecalData.FadeOutTime);
	}

	//sound
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactData.Sound, hit.ImpactPoint);
}

