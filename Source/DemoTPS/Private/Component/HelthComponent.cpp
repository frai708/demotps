// Demo project for portfolio. Lev Lobachev 


#include "HelthComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/Controller.h"
#include "DemoTPS/MainGameMode.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Perception/AISense_Damage.h"



DEFINE_LOG_CATEGORY_STATIC(LogHelthComponent, All,All);

UHelthComponent::UHelthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UHelthComponent::BeginPlay()
{
	Super::BeginPlay();
	check(MaxHealth > 0);
	SetHealth(MaxHealth);
	AActor* ComponentOwner = GetOwner();
	if(ComponentOwner)
	{
		ComponentOwner->OnTakeAnyDamage.AddDynamic(this, &ThisClass::OnTakeAnyDamageHandle);
		ComponentOwner->OnTakePointDamage.AddDynamic(this, &ThisClass::OnTakePointDamageHandle);
		ComponentOwner->OnTakeRadialDamage.AddDynamic(this, &ThisClass::OnTakeRadialDamageHandle);
	}
	
}
void UHelthComponent::OnTakeAnyDamageHandle(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	AController* InstigatedBy, AActor* DamageCauser)
{
	UE_LOG(LogHelthComponent, Display, TEXT("On Point damage: %f "), Damage);
}

void UHelthComponent::OnTakePointDamageHandle(AActor* DamagedActor, float Damage, AController* InstigatedBy,
	FVector HitLocation, UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection,
	const UDamageType* DamageType, AActor* DamageCauser)
{
	const auto FinalDamage = Damage * GetPointDamageModifier(DamagedActor, BoneName);
	UE_LOG(LogHelthComponent, Display, TEXT("On Point damage: %f,final damage: %f, boneName: %s "), Damage, FinalDamage, *BoneName.ToString());
	ApplyDamage(FinalDamage, InstigatedBy);
}

void UHelthComponent::OnTakeRadialDamageHandle(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	FVector Origin, const FHitResult& HitInfo, AController* InstigatedBy, AActor* DamageCauser)
{
	UE_LOG(LogHelthComponent, Display, TEXT("On Point damage: %f "), Damage);
	ApplyDamage(Damage, InstigatedBy);
}

void UHelthComponent::HealUpdate()
{
	SetHealth(Health+HealModifier);

	if(IsHealFull() && GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(HealTimerHandel);
	}
}

void UHelthComponent::SetHealth(float NewHealth)
{
	const auto NextHealth = FMath::Clamp(NewHealth, 0.f, MaxHealth);
	const auto HealthDelta = NextHealth - Health;
	Health = FMath::Clamp(NewHealth, 0 , MaxHealth);
	OnHealthChanged.Broadcast(Health, HealthDelta);
}

void UHelthComponent::PlayCameraShake()
{
	if(IsDead()) return;

	const auto Player = Cast<APawn>(GetOwner());
	if(!Player) return;

	const auto Controller = Player->GetController<APlayerController>();
	if(!Controller || !Controller->PlayerCameraManager) return;

	Controller->PlayerCameraManager->StartCameraShake(CameraShake);
}

void UHelthComponent::Killed(AController* KillerController)
{
	if(!GetWorld())return;
	const auto GameMode = Cast<AMainGameMode>(GetWorld()->GetAuthGameMode());

	if(!GameMode) return;

	const auto Player= Cast<APawn>(GetOwner());
	const auto VictimController = Player ? Player->Controller : nullptr;
	GameMode->Killed(KillerController, VictimController);
}

void UHelthComponent::ApplyDamage(float Damage, AController* InstigatedBy)
{
	if(Damage <= 0.f || IsDead() || !GetWorld())return;
	SetHealth(Health-Damage);
	if(IsDead())
	{
		Killed(InstigatedBy);
		OnDeath.Broadcast();
	}
	else if(bAutoHeal)
	{
		GetWorld()->GetTimerManager().SetTimer(HealTimerHandel, this, &UHelthComponent::HealUpdate, HealUpdateTime, true, HealDelay);
	}
	PlayCameraShake();
	ReportDamageEvent(Damage, InstigatedBy);
}

float UHelthComponent::GetPointDamageModifier(AActor* DamageActor, const FName& BoneName)
{
	const auto Character = Cast<ACharacter>(DamageActor);
	if(!Character || !Character->GetMesh() || !Character->GetMesh()->GetBodyInstance(BoneName)) return 1.f;

	const auto PhysMaterial = Character->GetMesh()->GetBodyInstance(BoneName)->GetSimplePhysicalMaterial();
	if(!PhysMaterial || !DamageModifiers.Contains(PhysMaterial)) return  1.f;

	return DamageModifiers[PhysMaterial];
}

void UHelthComponent::ReportDamageEvent(float Damage, AController* InstigateBy)
{
	if(!InstigateBy || !InstigateBy->GetPawn() || !GetOwner()) return;
	UAISense_Damage::ReportDamageEvent(GetWorld(),
						   GetOwner(),
								InstigateBy->GetPawn(),
								       Damage,
							InstigateBy->GetPawn()->GetActorLocation(),
							  GetOwner()->GetActorLocation() );
}

bool UHelthComponent::TryToAddHealth(float HealthAmount)
{
	if(IsDead() || IsHealFull()) return false;

	SetHealth(Health + HealthAmount);
	return true;
}

bool UHelthComponent::IsHealFull() const
{
	return FMath::IsNearlyEqual(Health, MaxHealth) && GetWorld();
}







