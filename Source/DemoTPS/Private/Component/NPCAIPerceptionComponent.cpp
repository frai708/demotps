// Demo project for portfolio. Lev Lobachev 


#include "NPCAIPerceptionComponent.h"
#include "AIController.h"
#include "DemoTPSUtils.h"
#include "HelthComponent.h"
#include "Perception/AISense_Sight.h"
#include "Perception/AISense_Damage.h"

AActor* UNPCAIPerceptionComponent::GetClosestEnemy() const
{
	TArray<AActor*> PercieveActors;

	GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), PercieveActors);
	if(PercieveActors.Num() == 0)
	{
		GetCurrentlyPerceivedActors(UAISense_Damage::StaticClass(), PercieveActors);
		if(PercieveActors.Num() == 0) return nullptr;
	}

	const auto TController = Cast<AAIController>(GetOwner());
	if(!TController) return nullptr;

	const auto Pawn = TController->GetPawn();
	if(!Pawn) return nullptr;

	float BestDistance = MAX_FLT;
	AActor* BestPawn = nullptr;
	for(const auto PercieveActor : PercieveActors)
	{
		const auto HealthComponent = FDemoTPSUtils::GetDemoPlayerComponent<UHelthComponent>(PercieveActor);
		const auto PercivePawn = Cast<APawn>(PercieveActor);
		const auto AreEnemies = PercivePawn && FDemoTPSUtils::AreEnemies(TController, PercivePawn->Controller);
		if(HealthComponent && !HealthComponent->IsDead() && AreEnemies)
		{
			const auto CurrentDistance = (PercieveActor->GetActorLocation() - Pawn->GetActorLocation()).Size();
			if(CurrentDistance < BestDistance)
			{
				BestDistance = CurrentDistance;
				BestPawn = PercieveActor;
			}
		}
	}
	
	return BestPawn;
}
