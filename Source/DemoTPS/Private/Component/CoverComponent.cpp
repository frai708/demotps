// Demo project for portfolio. Lev Lobachev 


#include "Component/CoverComponent.h"
#include "Player/PlayerCharacter.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/CapsuleComponent.h"
#include "DemoTPS/Component/TPSCharacterMovementComponent.h"

// Sets default values for this component's properties
UCoverComponent::UCoverComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	LatentInfo.CallbackTarget = this;
	LatentInfo.ExecutionFunction = "";
	LatentInfo.UUID = 12345;
	
}


// Called when the game starts
void UCoverComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UCoverComponent::TakeCover()
{
	const auto Character = Cast<APlayerCharacter>(GetOwner());
	if (!Character) return;
	const auto StartTrace = Character->GetActorLocation();
	const auto EndTrace = (UKismetMathLibrary::GetForwardVector(Character->GetActorRotation()) * 150) + StartTrace;
	FHitResult HitResult;
	if (UKismetSystemLibrary::SphereTraceSingleForObjects(GetWorld(), StartTrace, EndTrace, 20, ObjectTypes, false, ActorsToIgnore, EDrawDebugTrace::ForDuration, HitCover, true))
	{
		SetParametrsForCover(false, false, 400.f);
		const auto RotX = UKismetMathLibrary::MakeRotFromX(HitCover.Normal);
		const auto RelativeLocation = HitCover.Location + (UKismetMathLibrary::GetForwardVector(RotX) * 15.f);
		const auto RelativeRotation = UKismetMathLibrary::NormalizedDeltaRotator(RotX, FRotator(0.f, 180.f, 0.f));

		UKismetSystemLibrary::MoveComponentTo(Cast<USceneComponent>(Character->GetCapsuleComponent()), RelativeLocation, RelativeRotation, false, false, 0.3f, false, EMoveComponentAction::Move, LatentInfo);
	}

}

void UCoverComponent::SetParametrsForCover(bool OrientRotMovement, bool RotationYaw, float speed)
{
	const auto Character = Cast<APlayerCharacter>(GetOwner());
	if (!Character) return;
	const auto MovementComponent = Cast<UTPSCharacterMovementComponent>(Character->GetCharacterMovement());
	if (!MovementComponent) return;
	MovementComponent->bOrientRotationToMovement = OrientRotMovement;
	MovementComponent->MaxWalkSpeed = speed;
	Character->bUseControllerRotationYaw = RotationYaw;
}

bool UCoverComponent::DetectMovementInCover(float direction)
{
	const auto StartTrace = GetOwner()->GetActorLocation() + (UKismetMathLibrary::GetRightVector(GetOwner()->GetActorRotation()) * (direction * 25.f));
	const auto EndTrace = (UKismetMathLibrary::GetForwardVector(GetOwner()->GetActorRotation()) * 60) + StartTrace;
	if (UKismetSystemLibrary::SphereTraceSingleForObjects(GetWorld(), StartTrace, EndTrace, 10, ObjectTypes, false, ActorsToIgnore, EDrawDebugTrace::ForDuration, HitCover, true))
	{
		return true;
	}
	return false;
}

void UCoverComponent::QuitCover()
{
	SetParametrsForCover(true, true, 600.f);
}


// Called every frame
void UCoverComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

