// Demo project for portfolio. Lev Lobachev 


#include "WeaponComponent.h"
#include "BaseWeapon.h"
#include "InputActionValue.h"
#include "GameFramework/Character.h"
#include "Animations/EquipFinishNotify.h"
#include "Animations/RloadFinishedNotify.h"
#include "Animations/AnimUtils.h"

DEFINE_LOG_CATEGORY_STATIC(LogWeaponComponent, All,All)

constexpr static int32 GWeapon_Num =2;

UWeaponComponent::UWeaponComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UWeaponComponent::BeginPlay()
{
	Super::BeginPlay();

	checkf(WeaponData.Num() == 2, TEXT("Our character can hold only %i weapon item"), GWeapon_Num);
	
	CurrentWeaponIndex = 0;
	InitAnimaions();
	SpawnWeapons();
	EquipWeapon(CurrentWeaponIndex);
}

void UWeaponComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	CurrentWeapon = nullptr;
	for(auto Weapon : Weapons)
	{
		Weapon->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		Weapon->Destroy();
	}
	Weapons.Empty();
	Super::EndPlay(EndPlayReason);
}

void UWeaponComponent::StartFire()
{
	if(!CanFire()) return;
	CurrentWeapon->StartFire();
}

void UWeaponComponent::StopFire()
{
	if(!CurrentWeapon) return;
	CurrentWeapon->StopFire();
}

void UWeaponComponent::NextWeapon()
{
	if(!CanEquip())return;
	CurrentWeaponIndex = (CurrentWeaponIndex + 1) % Weapons.Num();
	EquipWeapon(CurrentWeaponIndex);
}

void UWeaponComponent::Reload()
{
	ChangeClip();
}

bool UWeaponComponent::GetWeaponUIData(FWeaponUIData& UIData) const
{
	if(CurrentWeapon)
	{
		UIData = CurrentWeapon->GetUIData();
		return true;	
	}
	return false;
}

bool UWeaponComponent::GetWeaponAmmoData(FAmmoData& AmmoData) const
{
	if(CurrentWeapon)
	{
		AmmoData = CurrentWeapon->GetAmmoData();
		return true;	
	}
	return false;
}

bool UWeaponComponent::TryToAddAmmo(TSubclassOf<ABaseWeapon> WeaponType, int32 ClipsAmount)
{
	for(const auto weapon : Weapons)
	{
		if(weapon && weapon->IsA(WeaponType))
		{
			return weapon->TryToAddAmmo(ClipsAmount);
		}
	}
	return false;
}

bool UWeaponComponent::TryToAddWeapon(TSubclassOf<ABaseWeapon> WeaponType)
{
	ACharacter* Character = Cast<ACharacter>(GetOwner());
	if (CurrentWeapon)
	{
		int32 CurrentIndex = Weapons.Find(CurrentWeapon);
		if (WeaponData[CurrentIndex].WeaponClass == WeaponType) return false;

		WeaponData[CurrentIndex].WeaponClass = WeaponType;
		CurrentWeapon->Destroy();
		auto Weapon = GetWorld()->SpawnActor<ABaseWeapon>(WeaponType);
		if (!Weapon) return false;
		Weapon->OnClipEmpty.AddUObject(this, &UWeaponComponent::OnEmptyClip);
		Weapon->SetOwner(Character);
		
		
		Weapons[CurrentIndex]->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		Weapons[CurrentIndex]->Destroy();
		Weapons[CurrentIndex] = Weapon;

		InitAnimaions();
		EquipWeapon(CurrentIndex);
		return true;

	}
	return false;
}

bool UWeaponComponent::TryToReloadAllWeapon(int32 ClipsAmount)
{
	if (!Weapons.IsEmpty())
	{
		for (auto weapon : Weapons)
		{
			weapon->TryToAddAmmo(ClipsAmount);
		}
		return true;
	}
	return false;
}

bool UWeaponComponent::NeedAmmo(TSubclassOf<ABaseWeapon> WeaponType)
{
	for(const auto weapon : Weapons)
	{
		if(weapon && weapon->IsA(WeaponType))
		{
			return !weapon->isAmmoFull();
		}
	}
	return false;
}


void UWeaponComponent::AttachWeaponToSocket(ABaseWeapon* Weapon, USceneComponent* SceneComponent,
                                            const FName& SocketName)
{
	if(!Weapon || !SceneComponent) return;
		FAttachmentTransformRules AttachmentRulls(EAttachmentRule::SnapToTarget, false);
    	Weapon->AttachToComponent(SceneComponent, AttachmentRulls, SocketName);
    	
}

void UWeaponComponent::EquipWeapon(int32 WeaponIndex)
{
	if(WeaponIndex < 0 || WeaponIndex>=Weapons.Num())
	{
		UE_LOG(LogWeaponComponent, Warning, TEXT("Invalid weapon index"));
	}
	ACharacter* Character = Cast<ACharacter>(GetOwner());
	if(!Character) return;

	if(CurrentWeapon)
	{
		UE_LOG(LogWeaponComponent, Display, TEXT("Current weapon valid"));
		CurrentWeapon->Zoom(false);
		CurrentWeapon->StopFire();
		AttachWeaponToSocket(CurrentWeapon, Character->GetMesh(), WeaponArmourySocketName);
	}
	CurrentWeapon = Weapons[WeaponIndex];
	//CurrentReloadAnimMontage = WeaponData[WeaponIndex].ReloadAnimMontage;
	const auto CurrentWeaponData = WeaponData.FindByPredicate([&](const FWeaponData& Data){return Data.WeaponClass == CurrentWeapon->GetClass();});

	CurrentReloadAnimMontage = CurrentWeaponData ? CurrentWeaponData->ReloadAnimMontage : nullptr;
	AttachWeaponToSocket(CurrentWeapon, Character->GetMesh(), WeaponEquipSocketName);
	bEquipAnimInProgress = true;
	PlayAnimMontage(EquipAnimMontage);
}

void UWeaponComponent::PlayAnimMontage(UAnimMontage* Animation)
{
		ACharacter* Character = Cast<ACharacter>(GetOwner());
		if(!Character) return;

		Character->PlayAnimMontage(Animation);
}

void UWeaponComponent::InitAnimaions()
{
	
	auto EquipFinishedNotify = AnimUtils::FindNotifyByClass<UEquipFinishNotify>(EquipAnimMontage);
	if(EquipFinishedNotify)
	{
		EquipFinishedNotify->OnNotified.AddUObject(this, &UWeaponComponent::OnEquipFinshed);
		
	}
	else
	{
		UE_LOG(LogWeaponComponent, Error, TEXT("Equip anim notify is forgotten to set"));
		checkNoEntry();
	}

	for(auto OneWeaponData : WeaponData)
	{
		auto ReloadFinishedNotify = AnimUtils::FindNotifyByClass<URloadFinishedNotify>(OneWeaponData.ReloadAnimMontage);
		if(!ReloadFinishedNotify)
		{
			UE_LOG(LogWeaponComponent, Error, TEXT("Equip anim notify is forgotten to set"));
			checkNoEntry();
		}
		
		ReloadFinishedNotify->OnNotified.AddUObject(this, &UWeaponComponent::OnReloadFinished);
	}
}

void UWeaponComponent::OnEquipFinshed(USkeletalMeshComponent* MeshComponent)
{

	ACharacter* Character = Cast<ACharacter>(GetOwner());
	
	if(!Character || MeshComponent != Character->GetMesh()) return;

	bEquipAnimInProgress = false;
	
	
}

void UWeaponComponent::OnReloadFinished(USkeletalMeshComponent* MeshComponent)
{
	ACharacter* Character = Cast<ACharacter>(GetOwner());
	
	if(!Character || MeshComponent != Character->GetMesh()) return;

	bRealoadAnimProgress = false;
	
}

void UWeaponComponent::OnEmptyClip(ABaseWeapon* AmmoEmptyWeapon)
{
	if(!AmmoEmptyWeapon) return;
	if(CurrentWeapon == AmmoEmptyWeapon)
	ChangeClip();
	else
	{
		for(const auto Weapon : Weapons)
		{
			if(Weapon == AmmoEmptyWeapon)
			{
				Weapon->ChangeClip();
			}
		}
	}
}

void UWeaponComponent::ChangeClip()
{
	if(!CanReload()) return;
	CurrentWeapon->StopFire();
	CurrentWeapon->ChangeClip();
	bRealoadAnimProgress = true;
	PlayAnimMontage(CurrentReloadAnimMontage);
}

void UWeaponComponent::Zoom(bool Enabled)
{
	if(CurrentWeapon)
	{
		CurrentWeapon->Zoom(Enabled);
		bIsZooming = Enabled;
	}
}

void UWeaponComponent::Zoom(const FInputActionValue& Value)
{
	if(CurrentWeapon)
	{
		CurrentWeapon->Zoom(Value.Get<bool>());
		bIsZooming = Value.Get<bool>();
	}
}

bool UWeaponComponent::getIsZooming() const
{
	return bIsZooming;
}

float UWeaponComponent::GetCurrentWeaponSensetivity() const
{
	if (!CurrentWeapon) return 0.0f;

	return CurrentWeapon->GetSensetivity();
}

bool UWeaponComponent::CanFire() const
{
	return CurrentWeapon && !bEquipAnimInProgress && !bRealoadAnimProgress;
}

bool UWeaponComponent::CanEquip() const
{
	return !bEquipAnimInProgress && !bRealoadAnimProgress;
}

bool UWeaponComponent::CanReload() const
{
	return CurrentWeapon && !bEquipAnimInProgress && !bRealoadAnimProgress && CurrentWeapon->CanReload();
}

void UWeaponComponent::SpawnWeapons()
{
	ACharacter* Character = Cast<ACharacter>(GetOwner());
	if(!GetWorld() || !Character) return;

	for(auto OneWeaponData : WeaponData)
	{
		auto Weapon = GetWorld()->SpawnActor<ABaseWeapon>(OneWeaponData.WeaponClass);
		if(!Weapon) continue;

		Weapon->OnClipEmpty.AddUObject(this, &UWeaponComponent::OnEmptyClip);
		Weapon->SetOwner(Character);
		Weapons.Add(Weapon);

		AttachWeaponToSocket(Weapon, Character->GetMesh(), WeaponArmourySocketName);
	}

}


