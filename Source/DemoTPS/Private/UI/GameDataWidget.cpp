// Demo project for portfolio. Lev Lobachev 


#include "UI/GameDataWidget.h"
#include "DemoTPS/MainGameMode.h"
#include "TPSPlayerState.h"

int32 UGameDataWidget::GetCurrentRoundNum() const
{
	const auto GameMode = GetMainGameMode();
	return GameMode ? GameMode->GetCurrentRoundNum() : 0;
}

int32 UGameDataWidget::GetTotalRoundNum() const
{
	const auto GameMode = GetMainGameMode();
	return GameMode ? GameMode->GetGameData().RoundNum : 0;
}

int32 UGameDataWidget::GetRoundSeconsRemaining() const
{
	const auto GameMode = GetMainGameMode();
	return GameMode ? GameMode->GetRoundSecondsRemaining() : 0;
}

AMainGameMode* UGameDataWidget::GetMainGameMode() const
{
	return GetWorld() ? Cast<AMainGameMode>(GetWorld()->GetAuthGameMode()) : nullptr;
}

ATPSPlayerState* UGameDataWidget::GetTPSPlayerState() const
{
	return GetOwningPlayer() ? Cast<ATPSPlayerState>(GetOwningPlayer()->PlayerState) : nullptr;
}
