// Demo project for portfolio. Lev Lobachev 


#include "UI/GameHUD.h"
#include "Engine/Canvas.h"
#include "DemoTPS/MainGameMode.h"
#include "UI/BaseWidget.h"
DEFINE_LOG_CATEGORY_STATIC(LogGameHUD, All, All);

void AGameHUD::DrawHUD()
{
	Super::DrawHUD();

	//DrawCrossHair();
}

void AGameHUD::BeginPlay()
{
	Super::BeginPlay();

	GameWidgets.Add(EMatchState::InProgress, CreateWidget<UBaseWidget>(GetWorld(), PlayerHUDWidgetClass));
	GameWidgets.Add(EMatchState::Pause, CreateWidget<UBaseWidget>(GetWorld(), PauseWidgetClass));
	GameWidgets.Add(EMatchState::GameOver, CreateWidget<UBaseWidget>(GetWorld(), GameOverWidgetClass));

	for(auto GameWidgetPair : GameWidgets)
	{
		const auto GameWidget = GameWidgetPair.Value;
		if(!GameWidget) continue;

		GameWidget->AddToViewport();
		GameWidget->SetVisibility(ESlateVisibility::Hidden);
	}
	
	if(GetWorld())
	{
		const auto GameMode = Cast<AMainGameMode>(GetWorld()->GetAuthGameMode());
		if(GameMode)
		{
			GameMode->OnMatchChangeState.AddUObject(this, &AGameHUD::OnMatchStateChanged);
		}
	}
}

void AGameHUD::DrawCrossHair()
{
	int32 SizeX = Canvas->SizeX;
	int32 SizeY = Canvas->SizeY;
	const TInterval<float> Center(SizeX * 0.5f, SizeY * 0.5);
	const float HalfLineSize = 10.f;
	const float LineThickness = 2.f;
	const FLinearColor LineColor = FLinearColor::Green;
	DrawLine(Center.Min - HalfLineSize, Center.Max, Center.Min + HalfLineSize, Center.Max, LineColor, LineThickness);
	DrawLine(Center.Min, Center.Max - HalfLineSize, Center.Min, Center.Max + HalfLineSize, LineColor, LineThickness);
}

void AGameHUD::OnMatchStateChanged(EMatchState State)
{
	if(CurrentWidget)
	{
		CurrentWidget->SetVisibility(ESlateVisibility::Hidden);
	}
	if(GameWidgets.Contains(State))
	{
		CurrentWidget = GameWidgets[State];
	}
	if(CurrentWidget)
	{
		CurrentWidget->SetVisibility(ESlateVisibility::Visible);
		CurrentWidget->Show();
	}
	UE_LOG(LogGameHUD, Display, TEXT("Match state changed %s: "), *UEnum::GetValueAsString(State));
}
