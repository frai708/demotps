// Demo project for portfolio. Lev Lobachev 


#include "UI/SpectatorWidget.h"
#include "DemoTPSUtils.h"
#include "RespawnComponent.h"

bool USpectatorWidget::GetRespawnTime (int32& CountDownTime) const
{
	const auto RespawnComponent = FDemoTPSUtils::GetDemoPlayerComponent<URespawnComponent>(GetOwningPlayer());
	if(!RespawnComponent || !RespawnComponent->IsRespawnInProgress()) return false;

	CountDownTime = RespawnComponent->GetRespawnCountDown();
	return true;
}
