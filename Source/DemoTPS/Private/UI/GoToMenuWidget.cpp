// Demo project for portfolio. Lev Lobachev 


#include "UI/GoToMenuWidget.h"
#include "Components/Button.h"
#include "TPSGameInstance.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogGOTOMENUWidget, All, All);

void UGoToMenuWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	
	if(GoToMenuButton)
	{
		GoToMenuButton->OnClicked.AddDynamic(this, &ThisClass::OnGoToMenu);
 	}
}

void UGoToMenuWidget::OnGoToMenu()
{
	if(!GetWorld()) return;

	const auto TPSGameInstance = GetWorld()->GetGameInstance<UTPSGameInstance>();
	if(!TPSGameInstance) return;

	if(TPSGameInstance->GetStartupLevel().LevelName.IsNone())
	{
		UE_LOG(LogGOTOMENUWidget, Error,TEXT("Menu level name is NONE"));
		return;
	}
	
	UGameplayStatics::OpenLevel(this, TPSGameInstance->GetMenuLevelName());

}
