// Demo project for portfolio. Lev Lobachev 


#include "UI/PlayerStatRawWidget.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"

void UPlayerStatRawWidget::SetPlayerName(const FText& Text)
{
	if(!PlayerNameTextBlock) return;

	PlayerNameTextBlock->SetText(Text);
}

void UPlayerStatRawWidget::SetKills(const FText& Text)
{
	if(!KillsTextBlock) return;

	KillsTextBlock->SetText(Text);
}

void UPlayerStatRawWidget::SetDeath(const FText& Text)
{
	if(!DeathTextBlock) return;

	DeathTextBlock->SetText(Text);
}

void UPlayerStatRawWidget::SetTeam(const FText& Text)
{
	if(!TeamTextBlock) return;

	TeamTextBlock->SetText(Text);
}

void UPlayerStatRawWidget::SetPlayerIndicatorVisability(bool Visible)
{
	if(!PlayerIndicatorImage) return;

	PlayerIndicatorImage->SetVisibility(Visible ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
}

void UPlayerStatRawWidget::SetTeamColor(const FLinearColor& Color)
{
	if(!TeamImage) return;
	TeamImage->SetColorAndOpacity(Color);
}
