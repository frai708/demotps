// Demo project for portfolio. Lev Lobachev 


#include "UI/PauseWidget.h"
#include "DemoTPS/MainGameMode.h"
#include "Components/Button.h"

void UPauseWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if(ClearPauseButton)
	{
		ClearPauseButton->OnClicked.AddDynamic(this, &ThisClass::OnClearPause);
	}
	
}

void UPauseWidget::OnClearPause()
{
	if(!GetWorld() || !GetWorld()->GetAuthGameMode()) return;

	GetWorld()->GetAuthGameMode()->ClearPause();
}
