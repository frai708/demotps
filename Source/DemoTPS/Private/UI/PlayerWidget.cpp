// Demo project for portfolio. Lev Lobachev 


#include "PlayerWidget.h"
#include "Component/HelthComponent.h"
#include "Component/WeaponComponent.h"
#include "DemoTPSUtils.h"
#include "Components/ProgressBar.h"
#include "TPSPlayerState.h"

void UPlayerWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	if(GetOwningPlayer())
	{
		GetOwningPlayer()->GetOnNewPawnNotifier().AddUObject(this, &UPlayerWidget::OnNewPawn);
		OnNewPawn(GetOwningPlayerPawn());
	}
}

float UPlayerWidget::GetHealthPercent() const
{
	const auto HealthComponent = FDemoTPSUtils::GetDemoPlayerComponent<UHelthComponent>(GetOwningPlayerPawn());
	if(!HealthComponent) return 0.f;

	return HealthComponent->GetHealthProcent();
}

bool UPlayerWidget::GetWeaponUIData(FWeaponUIData& UIData) const
{

	const auto WeaponComponent =FDemoTPSUtils::GetDemoPlayerComponent<UWeaponComponent>(GetOwningPlayerPawn());
	if(!WeaponComponent) return false;

	return WeaponComponent->GetWeaponUIData(UIData);
}

bool UPlayerWidget::GetWeaponAmmoData(FAmmoData& AmmoData) const
{

	const auto WeaponComponent =FDemoTPSUtils::GetDemoPlayerComponent<UWeaponComponent>(GetOwningPlayerPawn());
	if(!WeaponComponent) return false;

	return WeaponComponent->GetWeaponAmmoData(AmmoData);
}

bool UPlayerWidget::IsPlayerAlive() const
{
	const auto HealthComponent = FDemoTPSUtils::GetDemoPlayerComponent<UHelthComponent>(GetOwningPlayerPawn());
	return HealthComponent && !HealthComponent->IsDead();
}

bool UPlayerWidget::IsPlayerSpectating() const
{
	const auto Controller = GetOwningPlayer();
	return Controller && Controller->GetStateName() == NAME_Spectating;
}

int32 UPlayerWidget::GetKillsNum() const
{
	const auto Controller = GetOwningPlayer();
	if(!Controller) return 0;

	const auto PlayerState = Cast<ATPSPlayerState>(Controller->PlayerState);
	return PlayerState ? PlayerState->GetKillsNum() : 0;
}

FString UPlayerWidget::FormatBullets(int32 BulletsNum) const
{
	const int32 MaxLen = 3;
	const TCHAR PrefixSymbol = '0';

	auto BulletStr = FString::FromInt(BulletsNum);
	const auto SymbolNumToAdd = MaxLen - BulletStr.Len();

	if(SymbolNumToAdd > 0)
	{
		BulletStr = FString::ChrN(SymbolNumToAdd, PrefixSymbol).Append(BulletStr);
	}

	return BulletStr;
}


void UPlayerWidget::OnHealthChange(float Health, float HealthDelta)
{
	if(HealthDelta < 0.f)
	{
		OnTakeDamage();

		if(!IsAnimationPlaying(AnimationDamage))
		{
			PlayAnimation(AnimationDamage);
		}
	}
	UpdateHealthBar();
}

void UPlayerWidget::OnNewPawn(APawn* pawn)
{
	const auto HealthComponent = FDemoTPSUtils::GetDemoPlayerComponent<UHelthComponent>(pawn);
	if(HealthComponent && !HealthComponent->OnHealthChanged.IsBoundToObject(this))
	{
		HealthComponent->OnHealthChanged.AddUObject(this, &UPlayerWidget::OnHealthChange);
	}
	UpdateHealthBar();
}

void UPlayerWidget::UpdateHealthBar()
{
	if(!HealthProgressBar) return;
	{
		HealthProgressBar->SetFillColorAndOpacity(GetHealthPercent() > PercentColorThreshold ? GoodColor : BadColor);
	}
}


