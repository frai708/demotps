// Demo project for portfolio. Lev Lobachev 


#include "UI/GameOverWidget.h"
#include "DemoTPS/MainGameMode.h"
#include "Player/TPSPlayerState.h"
#include "PlayerStatRawWidget.h"
#include "DemoTPSUtils.h"
#include "Components/Button.h"
#include "Components/VerticalBox.h"
#include "Kismet/GameplayStatics.h"

void UGameOverWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	if(GetWorld())
	{
		const auto GameMode = Cast<AMainGameMode>(GetWorld()->GetAuthGameMode());
		if(GameMode)
		{
			GameMode->OnMatchChangeState.AddUObject(this, &ThisClass::OnMatchStateChanged);
		}
	}

	if(ResetLevelButton)
	{
		ResetLevelButton->OnClicked.AddDynamic(this, &ThisClass::OnResetLevel);
	}
}


void UGameOverWidget::OnMatchStateChanged(EMatchState State)
{
	if(State == EMatchState::GameOver)
	{
		UpdatePlayerState();
	}
}

void UGameOverWidget::UpdatePlayerState()
{
	if(!GetWorld() || !PlayerStatBox) return;

	PlayerStatBox->ClearChildren();
	
	for(auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		const auto Controller = It->Get();

		if(!Controller) continue;

		const auto PlayerState = Cast<ATPSPlayerState>(Controller->PlayerState);
		if(!PlayerState) continue;

		const auto PlayerStatRowWidget = CreateWidget<UPlayerStatRawWidget>(GetWorld(),PlayerStatRowWidgetClass);
		if(!PlayerStatRowWidget) continue;
		
		PlayerStatRowWidget->SetPlayerName(FText::FromString(PlayerState->GetPlayerName()));
		PlayerStatRowWidget->SetKills(FDemoTPSUtils::TextFromInt(PlayerState->GetKillsNum()));
		PlayerStatRowWidget->SetDeath(FDemoTPSUtils::TextFromInt(PlayerState->GetDeathNum()));
		PlayerStatRowWidget->SetTeam(FDemoTPSUtils::TextFromInt(PlayerState->GetTeamID()));
		PlayerStatRowWidget->SetPlayerIndicatorVisability(Controller->IsPlayerController());
		PlayerStatRowWidget->SetTeamColor(PlayerState->GetTeamColor());
		
		PlayerStatBox->AddChild(PlayerStatRowWidget);
	}
}

void UGameOverWidget::OnResetLevel()
{
	const FString CurrentLevelName = UGameplayStatics::GetCurrentLevelName(this);
	UGameplayStatics::OpenLevel(this, FName(CurrentLevelName));
}
