// Demo project for portfolio. Lev Lobachev 


#include "TPSGameInstance.h"
#include "Sound/SoundtFuncLib.h"

void UTPSGameInstance::ToggleVolume()
{
	USoundtFuncLib::ToggleSoundClassVolume(MasterSoundClass);
}
