// Demo project for portfolio. Lev Lobachev 


#include "TPSAIController.h"
#include "AICharacter.h"
#include "RespawnComponent.h"
#include "Component/NPCAIPerceptionComponent.h"
#include "BehaviorTree/BlackboardComponent.h"


ATPSAIController::ATPSAIController()
{
	NPCAIPerceptionComponent =CreateDefaultSubobject<UNPCAIPerceptionComponent>(TEXT("PerceptionComponent"));
	SetPerceptionComponent(*NPCAIPerceptionComponent);
	RespawnComponent = CreateDefaultSubobject<URespawnComponent>(TEXT("Respawn Component"));
	bWantsPlayerState = true;
}

void ATPSAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	const auto TCharacter = Cast<AAICharacter>(InPawn);
	if(TCharacter)
	{
		RunBehaviorTree(TCharacter->BehaviorTreeAsset);
	}
}

void ATPSAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	const auto AimActor = GetFocusOnActor();
	SetFocus(AimActor);
}

AActor* ATPSAIController::GetFocusOnActor()
{
	if(!GetBlackboardComponent()) return nullptr;
	return Cast<AActor>(GetBlackboardComponent()->GetValueAsObject(FocusOnKeyName));
}
