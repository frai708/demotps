// Demo project for portfolio. Lev Lobachev 


#include "AICharacter.h"
#include "TPSAIController.h"
#include "Component/AIWeaponComponent.h"
#include "BrainComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/WidgetComponent.h"
#include "UI/HealthBarWidget.h"
#include "HelthComponent.h"

AAICharacter::AAICharacter(const FObjectInitializer& Object)
: Super(Object.SetDefaultSubobjectClass<UAIWeaponComponent>("AIWeaponComponent"))
{
	AutoPossessAI = EAutoPossessAI::Disabled;
	AIControllerClass = ATPSAIController::StaticClass();

	bUseControllerRotationYaw = false;
	if(GetCharacterMovement())
	{
		GetCharacterMovement()->bUseControllerDesiredRotation = true;
		GetCharacterMovement()->RotationRate = FRotator(0.f,200.f, 0.f);
	}

	HealthWidgetComponent = CreateDefaultSubobject<UWidgetComponent>("HealthWidgetComponent");
	HealthWidgetComponent->SetupAttachment(GetRootComponent());
	HealthWidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
	HealthWidgetComponent->SetDrawAtDesiredSize(true);
}

void AAICharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	UpdateHealthWidgetVisibility();
}

void AAICharacter::BeginPlay()
{
	Super::BeginPlay();
	check(HealthWidgetComponent);
}

void AAICharacter::OnHealthChange(float health, float HealthDelta)
{
	Super::OnHealthChange(health, HealthDelta);

	const auto HealthBarWidget = Cast<UHealthBarWidget>(HealthWidgetComponent->GetUserWidgetObject());
	if(!HealthBarWidget) return;
	HealthBarWidget->SetHealthPercent(HealthComponent->GetHealthProcent());
}

void AAICharacter::OnDeath()
{
	Super::OnDeath();

	const auto TPSController = Cast<AAIController>(Controller);
	if(TPSController && TPSController->BrainComponent)
	{

		TPSController->BrainComponent->Cleanup();
	}
}

void AAICharacter::UpdateHealthWidgetVisibility()
{
	if(!GetWorld() || 
		!GetWorld()->GetFirstPlayerController() ||
		!GetWorld()->GetFirstPlayerController()->GetPawnOrSpectator()) return;
	const auto PlayerLocation = GetWorld()->GetFirstPlayerController()->GetPawnOrSpectator()->GetActorLocation();
	const auto Distance = FVector::Distance(PlayerLocation, GetActorLocation());
	HealthWidgetComponent->SetVisibility(Distance < HealthVisibilityDistance, true);
}
