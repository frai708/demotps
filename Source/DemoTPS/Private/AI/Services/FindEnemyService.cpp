// Demo project for portfolio. Lev Lobachev 


#include "FindEnemyService.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "DemoTPSUtils.h"
#include "Component/NPCAIPerceptionComponent.h"

UFindEnemyService::UFindEnemyService()
{
	NodeName = "FindEnemy";
}

void UFindEnemyService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	//UE_LOG(LogTemp, Display, TEXT("find enemy"));
	const auto Blackboard = OwnerComp.GetBlackboardComponent();
	if(Blackboard)
	{
		const auto TController = OwnerComp.GetAIOwner();
		const auto PerceptionComponent = FDemoTPSUtils::GetDemoPlayerComponent<UNPCAIPerceptionComponent>(TController);
		if(PerceptionComponent)
		{
			Blackboard->SetValueAsObject(EnemyActorKey.SelectedKeyName, PerceptionComponent->GetClosestEnemy());
		}
	}
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
