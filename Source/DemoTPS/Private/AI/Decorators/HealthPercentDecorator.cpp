// Demo project for portfolio. Lev Lobachev 

#include "HealthPercentDecorator.h"
#include "AIController.h"
#include "DemoTPSUtils.h"
#include "Component/HelthComponent.h"

UHealthPercentDecorator::UHealthPercentDecorator()
{
	NodeName = "Health Percent";
}

bool UHealthPercentDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	const auto Controller = OwnerComp.GetAIOwner();
	if(!Controller) return false;

	const auto HealthComponent = FDemoTPSUtils::GetDemoPlayerComponent<UHelthComponent>(Controller->GetPawn());
	if(!HealthComponent || HealthComponent->IsDead()) return false;
	
	return HealthComponent->GetHealthProcent() <= HealthPercent;
}
