// Demo project for portfolio. Lev Lobachev 


#include "NeedAmmoDecorator.h"
#include "AIController.h"
#include "DemoTPSUtils.h"
#include "Component/WeaponComponent.h"

UNeedAmmoDecorator::UNeedAmmoDecorator()
{
	NodeName = "Need Ammo";
}

bool UNeedAmmoDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	const auto Controller = OwnerComp.GetAIOwner();
	if(!Controller) return false;

	const auto WeaponComponent = FDemoTPSUtils::GetDemoPlayerComponent<UWeaponComponent>(Controller->GetPawn());

	if(!WeaponComponent) return false;

	return WeaponComponent->NeedAmmo(WeaponType);
}
