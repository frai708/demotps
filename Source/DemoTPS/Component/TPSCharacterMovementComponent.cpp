// Demo project for portfolio. Lev Lobachev 


#include "TPSCharacterMovementComponent.h"
#include "Player/BaseCharacter.h"

float UTPSCharacterMovementComponent::GetMaxSpeed() const
{
	const float MaxSpeed = Super::GetMaxSpeed();
	const ABaseCharacter* player = Cast<ABaseCharacter>(GetPawnOwner());
	return player && player->IsRunning() ? MaxSpeed *  RunModifier : MaxSpeed;
}
