//Demo project for portfolio. Lev Lobachev 


#include "MainGameMode.h"

#include "AIController.h"
#include "DemoPlayerController.h"
#include "BaseCharacter.h"
#include "GameHUD.h"
#include "Player/TPSPlayerState.h"
#include "DemoTPSUtils.h"
#include "RespawnComponent.h"
#include "WeaponComponent.h"
#include "TPSGameInstance.h"
#include "EngineUtils.h"

DEFINE_LOG_CATEGORY_STATIC(LogTPSGameMode, All, All);

constexpr static int32 MinRaundTimeForRespawn = 10;

AMainGameMode::AMainGameMode()
{
	DefaultPawnClass = ABaseCharacter::StaticClass();
	PlayerControllerClass = ADemoPlayerController::StaticClass();
	HUDClass = AGameHUD::StaticClass();
	PlayerStateClass = ATPSPlayerState::StaticClass();
}

void AMainGameMode::StartPlay()
{
	Super::StartPlay();

	auto TPSGameInstance = GetWorld()->GetGameInstance<UTPSGameInstance>();

	if (TPSGameInstance)
	{
		GameData.PlayersNum = TPSGameInstance->GetStartupLevel().PlayersCount;
		GameData.RoundNum = TPSGameInstance->GetStartupLevel().RoundCount;
		GameData.RoundTime = TPSGameInstance->GetStartupLevel().RoundTime;
	}

	SpawnBots();
	CreateTeamsInfo();
	CurrentRound = 1;
	StartRound();

	SetMatchState(EMatchState::InProgress);
}

UClass* AMainGameMode::GetDefaultPawnClassForController_Implementation(AController* InController)
{
	if(InController && InController->IsA<AAIController>())
	{
		return AIPawnClass;
	}
	return Super::GetDefaultPawnClassForController_Implementation(InController);
}

bool AMainGameMode::SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate)
{
	const auto PauseSet = Super::SetPause(PC, CanUnpauseDelegate);
	if(PauseSet)
	{
		StopAllFire();
		SetMatchState(EMatchState::Pause);
	}
	return Super::SetPause(PC, CanUnpauseDelegate);
}

bool AMainGameMode::ClearPause()
{
	const auto PauseCleared = Super::ClearPause();
	if(PauseCleared)
	{
			SetMatchState(EMatchState::InProgress);
	}
	return PauseCleared;
}

void AMainGameMode::Killed(AController* killerController, AController* VictimController)
{
	const auto KillerPlayerState = killerController ? Cast<ATPSPlayerState>(killerController->PlayerState) : nullptr;
	const auto VictimPlayerState = VictimController ? Cast<ATPSPlayerState>(VictimController->PlayerState) : nullptr;

	if(KillerPlayerState)
	{
		KillerPlayerState->AddKill();
	}

	if(VictimPlayerState)
	{
		VictimPlayerState->AddDeath();
	}
	StartRespawn(VictimController);
}

void AMainGameMode::RespawnRequest(AController* Controller)
{
	ResetOnePlayer(Controller);
}

void AMainGameMode::SpawnBots()
{
	if(!GetWorld()) return;

	for(int32 i=0;  i<GameData.PlayersNum -1; ++i)
	{
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		const auto AIcontroller = GetWorld()->SpawnActor<AAIController>(AIControllerClass, SpawnInfo);
		RestartPlayer(AIcontroller);
	}
}

void AMainGameMode::StartRound()
{
	RoundCountDown = GameData.RoundTime;
	GetWorldTimerManager().SetTimer(GameRoundTimerHandle, this, &AMainGameMode::GameTimerUpdate, 1.f, true);
}

void AMainGameMode::GameTimerUpdate()
{
	//UE_LOG(LogTPSGameMode, Display, TEXT("Time: %i / Round: %i/%i"), RoundCountDown, CurrentRound, GameData.RoundNum);
	
	if(--RoundCountDown == 0)
	{
		GetWorldTimerManager().ClearTimer(GameRoundTimerHandle);

		if(CurrentRound + 1 <= GameData.RoundNum)
		{
			++CurrentRound;
			ResetPlayers();
			StartRound();
		}
		else
		{
			GameOver();

		}
	}
}

void AMainGameMode::ResetPlayers()
{
	if(!GetWorld()) return;

	for(auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		ResetOnePlayer(It->Get());
	}
}

void AMainGameMode::ResetOnePlayer(AController* Controller)
{
	if(Controller && Controller->GetPawn())
	{
		Controller->GetPawn()->Reset();
	}
	RestartPlayer(Controller);
	SetPlayerColor(Controller);
}

void AMainGameMode::CreateTeamsInfo()
{
	if(!GetWorld()) return;

	int32 TeamID = 1;
	for(auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		const auto Controller = It->Get();
		if(!Controller)continue;

		const auto PlayerState = Cast<ATPSPlayerState>(Controller->PlayerState);
		if(!PlayerState) continue;

		PlayerState->SetTeamID(TeamID);
		PlayerState->SetTeamColor(DetermineColorByTeamID(TeamID));
		PlayerState->SetPlayerName(Controller->IsPlayerController() ? "Player" : "Bot");
		SetPlayerColor(Controller);
		TeamID = TeamID ==1 ?2:1;
	}
}

FLinearColor AMainGameMode::DetermineColorByTeamID(int32 TeamID) const
{
	if(TeamID -1 < GameData.TeamColors.Num())
	{
		return GameData.TeamColors[TeamID -1];
	}
	UE_LOG(LogTPSGameMode, Warning, TEXT("No color for team id: %i, set to default: %s"), TeamID, *GameData.DefaultTeamOnlyColor.ToString());
	return GameData.DefaultTeamOnlyColor;
}

void AMainGameMode::SetPlayerColor(AController* Controller)
{
	if(!Controller) return;

	const auto Character = Cast<ABaseCharacter>(Controller->GetPawn());
	if(!Character) return;

	const auto PlayerState = Cast<ATPSPlayerState>(Controller->PlayerState);
	if(!PlayerState) return;

	Character->SetPlayerColor(PlayerState->GetTeamColor());
}

void AMainGameMode::LogPlayerInfo()
{
	if(!GetWorld()) return;
	for(auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		const auto Controller = It->Get();
		if(!Controller)continue;

		const auto PlayerState = Cast<ATPSPlayerState>(Controller->PlayerState);
		if(!PlayerState) continue;

		PlayerState->LogInfo();
	}
}

void AMainGameMode::StartRespawn(AController* Controller)
{
	const auto respawnAvailable = RoundCountDown > MinRaundTimeForRespawn + GameData.RespawnTime;
	const auto RespawnComponent = FDemoTPSUtils::GetDemoPlayerComponent<URespawnComponent>(Controller);
	if(!RespawnComponent) return;

	RespawnComponent->Respawn(GameData.RespawnTime);
}

void AMainGameMode::GameOver()
{
	UE_LOG(LogTPSGameMode, Display, TEXT("============Game Over============"));
	LogPlayerInfo();

	for(auto Pawn:TActorRange<APawn>(GetWorld()))
	{
		if(Pawn)
		{
			Pawn->TurnOff();
			Pawn->DisableInput(nullptr);
		}
	}

	SetMatchState(EMatchState::GameOver);
}

void AMainGameMode::SetMatchState(EMatchState State)
{
	if(MatchState == State) return;

	MatchState = State;
	OnMatchChangeState.Broadcast(MatchState);
}

void AMainGameMode::StopAllFire()
{
	for(auto Pawn : TActorRange<APawn>(GetWorld()))
	{
		const auto WeaponComponent = FDemoTPSUtils::GetDemoPlayerComponent<UWeaponComponent>(Pawn);
		if(!WeaponComponent) continue;

		WeaponComponent->StopFire();
		WeaponComponent->Zoom(false);
		
	}
}
